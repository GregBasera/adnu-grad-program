// --------------------------------------------------------------------------------------------
// Filename   : study.js                                                                      |
// Author     : Dean Simon R. Damasig                                                         |
// Description: This script contains functions such as adding and dropping subjects,          |
//              passing PDF contents, and others.                                             |
// Date       : August 28, 2018
// --------------------------------------------------------------------------------------------

//-----------------------------------------------GREG-[add active property to sidebar]--
var currentUrl = window.location.href;
var partsUrl = currentUrl.split("/");
var partsFile = partsUrl[5].split(".");
var targetID = partsFile[0];

var classList = document.getElementById(targetID).classList;
classList.add("active");
//------------------------------------------------GREG---

// Global Variables
var globalDegree;
var lastDegree;
var globalTrack;
var lastTrack;
var schoolYearArray = ["2018 - 2019", "2019 - 2020", "2020 - 2021", "2021 - 2022", "2022 - 2023"];

// Load startup scripts
$(document).ready(function () {
    // Disable all <select> elements
    var firstSyy = $('#first-select-school-year');
    var subSyy = $('#dynamicWrapper .sub-select-school-year');
    var subSm = $('#dynamicWrapper .select-semester');

    subSyy.prop('disabled', 'disabled');
    subSm.prop('disabled', 'disabled');

    // Hide the add semester button
    $('.addSemester').hide();
});

// Disable right click
// $(document).bind("contextmenu",function(e) {
// 	e.preventDefault();
// });

// Disable inspect element
// $(document.onkeydown = function (e) {
//     // if (event.keyCode == 123) {
//     //     return false;
//     // }
//     if (e.ctrlKey && e.shiftKey && e.keyCode == 'I'.charCodeAt(0)) {
//         return false;
//     }
//     if (e.ctrlKey && e.shiftKey && e.keyCode == 'C'.charCodeAt(0)) {
//         return false;
//     }
//     if (e.ctrlKey && e.shiftKey && e.keyCode == 'J'.charCodeAt(0)) {
//         return false;
//     }
//     if (e.ctrlKey && e.keyCode == 'U'.charCodeAt(0)) {
//         return false;
//     }
// });

// Reenable the selects upon clicking Download PDF
$('#create-pdf').click(function () {
    // Disable all <select> elements
    var firstSyy = $('#first-select-school-year');
    var subSyy = $('#dynamicWrapper .sub-select-school-year');
    var subSm = $('#dynamicWrapper .select-semester');

    subSyy.prop('disabled', false);
    subSm.prop('disabled', false);
});

// Returns true if there are no added subjects
function isSubjectEmpty() {
    if ($('table tr.added-subject-row').length <= 0)
        return true;
    return false;
}

// Executes upon clicking anything
$(document).click( function() {
    // Check if the enlistment forms are empty
    if(isSubjectEmpty()){
        // Disable the Download PDF button
        $('#create-pdf').prop('disabled', 'disabled');
        // $('#print-assess').prop('disabled', 'disabled');
      }
    else{
        $('#create-pdf').prop('disabled', false);
        // $('#print-assess').prop('disabled', false);
      }
});

// Update the available subjects based on the DEGREE
function updateSubjects() {
    // Check if there are added subjecs
    if ($(".subject_table tr.added-subject-row").length > 0) {
        // Show a warning modal
        $("#refresh-warning-modal").modal("show");
    }
    // Get the selected degree
    var degree = $("#select-degree").find(":selected").val();
    // Update the glombal degree
    lastDegree = globalDegree;
    globalDegree = degree;
    if (lastDegree == undefined) {
        lastDegree = globalDegree;
    }
    // Check whether it's the default
    if (degree != "Choose...") {
        $("#addSubjectModal tr").addClass("d-none");
        $("tr.subject-modal-row-" + degree).removeClass("d-none");
        $('#subject-modal-header').removeClass('d-none');
    }
    // Hide all subjects
    else {
        $("#addSubjectModal tr").addClass("d-none");
        $('#subject-modal-header').addClass('d-none');
    }
}

// Update the available track subjects based on TRACK
function updateTrackSubjects(){
    // Check if there are added subjecs
    if ($(".subject_table tr.added-subject-row").length > 0) {
        // Show a warning modal
        $("#refresh-warning-modal").modal("show");
    }
    // Get the selected track
    var track = $("#select-track").find(":selected").val();
    console.log('Track: ' + track);
    // Update the global track
    lastTrack = globalTrack;
    globalTrack = track;
    if (lastTrack == undefined) {
        lastTrack = globalTrack;
    }
    // Check whether it's the default
    if (track != "Choose..." || track != "Choose track...") {
        $("#addSubjectModal tr.track-subject").addClass("d-none");
        $("tr.track-subject-row-" + track).removeClass("d-none");
        $('#subject-modal-header').removeClass('d-none');
    }
    // Hide track subjects
    else {
        $("#addSubjectModal tr.track-subject").addClass("d-none");
    }
}

// Shows/Hides subjects upon changing DEGREE
$("#select-degree").change(function () {
    updateSubjects();
});

// Shows/Hides track subjects upon changing TRACK
$("#select-track").change(function () {
    updateTrackSubjects();
});

// Shows/Hides TRACKS based on selected DEGREE
$("#select-degree").change(function () {
    // Get the selected degree
    var degree = $("#select-degree").find(":selected").val();

    // Hide all options
    var all_options = $("#select-track option");
    all_options.addClass("d-none");

    // Show the valid options
    try {
        var valid_options = $("#select-track .track-" + degree);
        valid_options.removeClass("d-none");
    } catch (err) {
        console.log("No Degree Selected...");
    }

    // Reset selection
    var default_option = $("#select-track option").first();
    default_option.removeClass("d-none");
    $("#select-track").prop("selectedIndex", 0);

    // Change the value of the default option
    if (degree != "Choose...")
        default_option.text("Choose track...");
    else
        default_option.text("Choose a degree first...");
});

// Resets the added subjects
$("#refresh-continue").click(function () {
    // Trigger all drop buttons
    $("button.drop").trigger("click");
    // Hide refresh modal
    $("#refresh-warning-modal").modal("hide");
    // Check whether it's the default
    if (globalDegree != "Choose...") {
        $("#addSubjectModal tr").addClass("d-none");
        $("tr.subject-modal-row-" + globalDegree).removeClass("d-none");
    }
    // Hide all subjects
    else {
        $("#addSubjectModal tr").addClass("d-none");
    }
});

// Adds a new semester <div>
$("a.addSemester").click(function () {
    // Clone the enlistment form template
    var template = $("#hiddenEnlistmentForm").clone();
    template.removeClass("d-none");
    template.addClass("d-block");
    template.attr("id", "");
    $("#hiddenEnlistmentForm").before(template);
});

// Shows/Hides <tr> depending on search queries
$(document).ready(function () {
    $("#searchBar").on("keyup", function () {
        var value = $(this).val().toLowerCase();
        $("#addSubjectModalBody tr").filter(function () {
            $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
        });
    });
});

// Changes the active table
$("#dynamicWrapper").on("click", "a", function () {
    var table = $(this).parents("table");
    // Change the active table
    $("#active-table").attr("id", "");
    table.attr('id', 'active-table');
});

// Add subjects from modal to main table
$("table").on("click", "button.add", function () {

    // Get the row element containing the subject information
    var row = $(this).parents("tr");

    // Get the columns of the current subject
    var columns = row.children();

    // Get the subject info
    var subjectCode = columns[0].innerHTML;
    var subjectTitle = columns[1].innerHTML;
    var subjectUnits = columns[2].innerHTML;
    var markup =
    "<tr class='added-subject-row' program='custom'>" +
    "<td>" + subjectCode + "</td>" +
    "<td>" + subjectTitle + "</td>" +
    "<td class='units-col'>" + subjectUnits + "</td>" +
    "<td class='action-column'>" +
    "	<button class='btn btn-sm btn-danger drop'>" +
    "			<i class='fa fa-times mr-1'></i>" +
    "			Drop" +
    " 	</button>" +
    "</td>" +
    "<td>" +
    "<input class='d-none' type='text' name='subject-code[]' value='" + subjectCode.trim() + "'>" +
    "<input class='d-none' type='text' name='subject-title[]' value='" + subjectTitle.trim() + "'>" +
    "<input class='d-none' type='text' name='subject-units[]' value='" + subjectUnits.trim() + "'> </td> </tr>";

    // Get the total units <tr>
    var lastRow = $("#active-table tr.total-units-row");

    // Append a row after the total units <tr>
    lastRow.before(markup);

    // Remove this row
    row.remove();

    // Update the total number of units for the active table
    updateTotalUnits();

    // Increment the subject counter
    incrementSubjectCounter();
});

// Increment the of the active table
function incrementSubjectCounter() {
    var subjectCounter = $('#active-table .subject-counter');
    subjectCounter.val(parseInt(subjectCounter.val()) + parseInt(1));
}

// Decrement the of the active table
function decrementSubjectCounter() {
    var subjectCounter = $('#active-table .subject-counter');
    subjectCounter.val(parseInt(subjectCounter.val()) - parseInt(1));
}

// Drops a subject from the active table to the modal
$("#dynamicWrapper").on("click", "button.drop", function () {
    // Get the row element containing the subject information
    var row = $(this).parents("tr");
    // Get the columns of the current subject
    var columns = row.children();
    // Get the subject info
    var subjectCode = columns[0].innerHTML;
    var subjectTitle = columns[1].innerHTML;
    var subjectUnits = columns[2].innerHTML;
    var markup = "<tr class='subject-modal-row-" + lastDegree + "'>" +
        "<td>" + subjectCode + "</td>" +
        "<td>" + subjectTitle + "</td>" +
        "<td>" + subjectUnits + "</td>" +
        "<td><button class='btn btn-sm btn-success add'>" +
        "			<i class='fa fa-plus mr-1'></i>" +
        "			Add" +
        " 	</button></td>" +
        "</tr>";

    // Get the last row of the active table
    var lastRow = $("#modal-table tr:last");
    // Append a row after the last subject row
    lastRow.after(markup);

    // Remove this row
    row.remove();

    // Recompute the totalUnits
    updateTotalUnits();

    // Decrement the subject counter
    decrementSubjectCounter();
})

// Prevents scrolling when empty <a> is clicked
$('a.cancelDefault').click(function (e) {
    // Cancel the default action
    e.preventDefault();
});

// Filters school year and semester <select>
function updateSchoolYear(firstSyy, subSyy, subSm) {
    var currentIndex = firstSyy.prop('selectedIndex');
    var counter = 3;
    subSyy.each(function (index, item) {
        counter--;
        if (counter <= 0) {
            currentIndex++;
            counter = 3;
        }

        updateSemester(subSm.eq(index), counter);
        subSyy.eq(index).prop('selectedIndex', currentIndex);
    });
}

// Updates the <select> elements for semester
function updateSemester(sm, counter) {
    // First Semester
    if (counter == 2)
        sm.prop('selectedIndex', 0);
    // Second Semester
    else if (counter == 1)
        sm.prop('selectedIndex', 1);
    // Summer Semester
    else
        sm.prop('selectedIndex', 2);
}

// Updates the totalUnits of the .total-units-col
function updateTotalUnits() {
    var unitsCols = $('#active-table .units-col');
    var hiddenTotalInputs = $('#active-table .hidden-input-total-units');
    var totalUnits = 0;
    if(unitsCols.length <= 0) {
        $('#active-table .total-units-col').html(0);
        $('#active-table .hidden-input-total-units').val(0);
        return false;
    }
    unitsCols.each(function (index, item) {
        totalUnits += parseInt(unitsCols.eq(index).html());
        console.log(totalUnits);
        $('#active-table .total-units-col').html(totalUnits);
        console.log('Hidden Input: ' + $('#active-table .hidden-input-total-units'));
        $('#active-table .hidden-input-total-units').val(parseInt(totalUnits));
    });
}

// Updates the <select> elements for school year and semester upon changing the first <select>
$('#dynamicWrapper').on('change', '#first-select-school-year', function () {
    var firstSyy = $('#first-select-school-year');
    var subSyy = $('#dynamicWrapper .sub-select-school-year');
    var subSm = $('#dynamicWrapper .select-semester');

    updateSchoolYear(firstSyy, subSyy, subSm);

    // Toggle addSemesterButton
    if (firstSyy.prop('selectedIndex') == 0)
        $('.addSemester').hide()
    else
        $('.addSemester').show()
});

// Updates the <select> elements for school year and semester upon adding a new semester
$('.addSemester').click(function () {
    var firstSyy = $('#first-select-school-year');
    var subSyy = $('#dynamicWrapper .sub-select-school-year');
    var subSm = $('#dynamicWrapper .select-semester');

    updateSchoolYear(firstSyy, subSyy, subSm);
});
