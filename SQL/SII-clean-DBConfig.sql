-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 08, 2018 at 01:10 PM
-- Server version: 10.1.30-MariaDB
-- PHP Version: 7.2.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sii`
--

-- --------------------------------------------------------

--
-- Table structure for table `fee`
--

CREATE TABLE `fee` (
  `fee_type` varchar(30) NOT NULL,
  `amount` double NOT NULL,
  `fee_code` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `fee`
--

INSERT INTO `fee` (`fee_type`, `amount`, `fee_code`) VALUES
('DIA Laboratory Fee', 6013.35, 'DIA_Laboratory_Fee'),
('EDP Laboratory Fee', 4129.35, 'EDP_Laboratory_Fee'),
('Energy Fee', 159.2, 'Energy_Fee'),
('None', 0, 'None');

-- --------------------------------------------------------

--
-- Table structure for table `program`
--

CREATE TABLE `program` (
  `program_code` varchar(30) NOT NULL,
  `program_name` varchar(100) NOT NULL,
  `program_details` varchar(500) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `program`
--

INSERT INTO `program` (`program_code`, `program_name`, `program_details`) VALUES
('MCGA', 'Master of Computer Graphics', 'About the Program Course'),
('MIT', 'Master of Information Technology', ' The MSIT degree is designed for those managing information technology, especially the information systems development process.'),
('MSCS', 'Master of Science in Computer Science ', 'MCS program provides a conceptual and practical education in computer science by combining a broad core curriculum with user-selected areas of study.');

-- --------------------------------------------------------

--
-- Table structure for table `subject`
--

CREATE TABLE `subject` (
  `subject_code` varchar(30) NOT NULL,
  `subject_name` varchar(150) NOT NULL,
  `units` int(11) NOT NULL,
  `fee_type` varchar(30) NOT NULL,
  `subject_type` varchar(30) NOT NULL,
  `program_code` varchar(30) NOT NULL,
  `room` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `subject`
--

INSERT INTO `subject` (`subject_code`, `subject_name`, `units`, `fee_type`, `subject_type`, `program_code`, `room`) VALUES
('BC101', 'Object-Oriented Programming', 3, 'Energy_Fee', 'brg', 'MIT', 'AL211A'),
('BC102', 'Data Structures and Algorithms', 3, 'Energy_Fee', 'brg', 'MIT', 'CSLAB2'),
('BC103', 'Database Systems', 3, 'Energy_Fee', 'brg', 'MIT', 'AL211A'),
('BCGA01', 'Object-Oriented Graphics Programming in C++', 3, 'Energy_Fee', 'brg', 'MCGA', ''),
('BCGA02', 'Mathematical Methods for Graphics and Animation', 3, 'None', 'brg', 'MCGA', ''),
('BCGA03', 'Computer Fundamentals for Artists', 3, 'None', 'brg', 'MCGA', ''),
('BCGA04', 'Story Concept, Character Design and Storyboard Development', 3, 'None', 'brg', 'MCGA', ''),
('BCGA05', '3D Modeling and Texturing', 3, 'None', 'brg', 'MCGA', ''),
('BCGA06', '3D Animation and Character Animation', 3, 'None', 'brg', 'MCGA', ''),
('BCGA07', 'Dynamics and Rendering', 3, 'None', 'brg', 'MCGA', ''),
('BCGA08', 'Non-linear Video and Audio Editing', 3, 'None', 'brg', 'MCGA', ''),
('BCGA09', 'Digital Compositing & Visual Effects', 3, 'None', 'brg', 'MCGA', ''),
('MCGA101', 'Digital Image Processing', 3, 'DIA_Laboratory Fee', 'c', 'MCGA', ''),
('MCGA102', 'Advanced Computer Graphics', 3, 'None', 'c', 'MCGA', ''),
('MCGA103', 'Story Development & Screenwriting', 3, 'None', 'c', 'MCGA', ''),
('MCGA104', 'Advanced 3D Digital Animation', 3, 'None', 'c', 'MCGA', ''),
('MCGA105', 'Digital Sound Design, Editing, and Production', 3, 'None', 'c', 'MCGA', ''),
('MCGA201', 'Creative Perception and Aesthetics Theory', 3, 'None', 'trk', 'MCGA', ''),
('MCGA202', 'Photography, Film, and Video', 3, 'None', 'trk', 'MCGA', ''),
('MCGA203', 'Professional Critiques, Critical Thinking, and Writing About the Arts', 3, 'None', 'trk', 'MCGA', ''),
('MCGA204', 'History of Art and Technology', 3, 'None', 'trk', 'MCGA', ''),
('MCGA205', 'History of animation and production', 3, 'None', 'trk', 'MCGA', ''),
('MCGA206', 'Business of Computer Graphics and Animation', 3, 'None', 'trk', 'MCGA', ''),
('MCGA211', 'Computer Vision Algorithms and Applications', 3, 'None', 'trk', 'MCGA', ''),
('MCGA212', 'Physical Computing, Modeling and Simulation', 3, 'None', 'trk', 'MCGA', ''),
('MCGA213', 'Algorithms in Art and Design', 3, 'None', 'trk', 'MCGA', ''),
('MCGA214', 'Augmented Reality techniques', 3, 'None', 'trk', 'MCGA', ''),
('MCGA215', 'Data Visualization', 3, 'None', 'trk', 'MCGA', ''),
('MCGA216', 'Game Programming, Development, and Production', 3, 'None', 'trk', 'MCGA', ''),
('MCGA221', 'Principles of Illustration, Design, and Production', 3, 'None', 'trk', 'MCGA', ''),
('MCGA222', 'Advanced Stop Motion Animation', 3, 'None', 'trk', 'MCGA', ''),
('MCGA223', 'Advanced 2D Digital Animation', 3, 'None', 'trk', 'MCGA', ''),
('MCGA224', 'Advanced Digital Audio', 3, 'None', 'trk', 'MCGA', ''),
('MCGA225', 'Advanced Digital Sculpture', 3, 'None', 'trk', 'MCGA', ''),
('MCGA226', 'Contemporary Topics in Animation', 3, 'None', 'trk', 'MCGA', ''),
('MCGA301', 'Thesis Project Development and Production I', 3, 'None', 'ths', 'MCGA', ''),
('MCGA302', 'Thesis Project Development and Production II', 6, 'None', 'ths', 'MCGA', ''),
('MITC01', 'Technology and Project Management', 3, 'Energy_Fee', 'c', 'MIT', 'AL211A'),
('MITC02', 'Advanced Database Systems', 3, 'Energy_Fee', 'c', 'MIT', ''),
('MITC03', 'Advanced System Design and Implementation', 3, 'Energy_Fee', 'c', 'MIT', ''),
('MITC04', 'Advanced Operating Systems and Networking', 3, 'Energy_Fee', 'c', 'MIT', ''),
('MITP01', 'Graduate Project 1', 6, 'EDP Laboratory Fee', 'ths', 'MIT', ''),
('MITP02', 'Graduate Project 2', 6, 'Energy_Fee', 'ths', 'MIT', ''),
('MITS01', 'Advanced Client-Server Computing', 3, 'Energy_Fee', 'trk', 'MIT', 'CSLAB1'),
('MITS02', 'Human Computer Interaction', 3, 'None', 'trk', 'MIT', ''),
('MITS03', 'Object-Oriented Design Patterns', 3, 'Energy_Fee', 'trk', 'MIT', 'CSLAB2'),
('MITS04', 'WEB Engineering', 3, 'Energy_Fee', 'trk', 'MIT', 'CSLAB2'),
('MITS05', 'Mobile Applications Development', 3, 'Energy_Fee', 'trk', 'MIT', 'CSLAB1'),
('MITS06', 'Trends in Systems Development', 3, 'Energy_Fee', 'trk', 'MIT', 'AL213'),
('MITS07', 'Business Process Re-engineering', 3, 'None', 'trk', 'MIT', 'AL213'),
('MITS08', 'Management Information Systems', 3, 'None', 'trk', 'MIT', ''),
('MITS09', 'Strategic Information Management', 3, 'None', 'trk', 'MIT', ''),
('MITS10', 'Network Administration & Maintenance', 3, 'Energy_Fee', 'trk', 'MIT', 'AL211A'),
('MITS11', 'Systems Infrastructure Deployment and Integration', 3, 'None', 'trk', 'MIT', ''),
('MITS12', 'Trends in Information Systems', 3, 'None', 'trk', 'MIT', ''),
('MSCSC01', 'Advanced Data Structures and Algorithms', 3, 'None', 'c', 'MSCS', ''),
('MSCSC02', 'Advanced Operating Systems and Computer Organization', 3, 'None', 'c', 'MSCS', ''),
('MSCSC03', 'Advanced Theory of Programming Languages', 3, 'Energy_Fee', 'c', 'MSCS', 'AL211B'),
('MSCSC04', 'Theory of Computation', 3, 'None', 'c', 'MSCS', ''),
('MSCSS01', 'Advanced Software Engineering', 3, 'None', 'trk', 'MSCS', ''),
('MSCSS02', 'Advanced Database Systems', 3, 'None', 'trk', 'MSCS', ''),
('MSCSS03', 'Object-Oriented Design Patterns', 3, 'None', 'trk', 'MSCS', ''),
('MSCSS04', 'Language Compiler and Construction', 3, 'None', 'trk', 'MSCS', ''),
('MSCSS05', 'Parallel Algorithms and Programming', 3, 'None', 'trk', 'MSCS', ''),
('MSCSS06', 'Current Trends in Applied Computing', 3, 'None', 'trk', 'MSCS', ''),
('MSCSS07', 'Computer Graphics', 3, 'None', 'trk', 'MSCS', ''),
('MSCSS08', 'Digital Image Processing', 3, 'None', 'trk', 'MSCS', ''),
('MSCSS09', 'Computer Vision', 3, 'None', 'trk', 'MSCS', ''),
('MSCSS10', 'Artificial Intelligence', 3, 'None', 'trk', 'MSCS', ''),
('MSCSS11', 'Data Visualization', 3, 'None', 'trk', 'MSCS', ''),
('MSCSS12', 'Trends in Graphics and Computer Vision', 3, 'None', 'trk', 'MSCS', ''),
('MSCST01', 'Graduate Thesis 1', 3, 'None', 'ths', 'MSCS', ''),
('MSCST02', 'Graduate Thesis 2', 3, 'None', 'ths', 'MSCS', ''),
('Sample1', 'Sample', 3, 'Energy_Fee', 'trk', 'MIT', '');

-- --------------------------------------------------------

--
-- Table structure for table `track`
--

CREATE TABLE `track` (
  `track_code` varchar(30) NOT NULL,
  `track_name` varchar(150) NOT NULL,
  `program_code` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `track`
--

INSERT INTO `track` (`track_code`, `track_name`, `program_code`) VALUES
('Apl_Cmp', 'Applied Computing', 'MSCS'),
('DAP', 'Digital Arts, Animation and Production', 'MCGA'),
('Gen', 'General', 'MCGA'),
('Grap_ComVis', 'Graphics and Computer Vision', 'MSCS'),
('Info_Sys', 'Information Systems', 'MIT'),
('SCGDDP', 'Scientific Computing, Game Design, Development, and Production', 'MCGA'),
('SYS_DEV', 'Systems Development', 'MIT');

-- --------------------------------------------------------

--
-- Table structure for table `track_subject`
--

CREATE TABLE `track_subject` (
  `id` int(150) NOT NULL,
  `subject_code` varchar(150) NOT NULL,
  `track_code` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `track_subject`
--

INSERT INTO `track_subject` (`id`, `subject_code`, `track_code`) VALUES
(1, 'MITS01', 'SYS_DEV'),
(2, 'MITS02', 'SYS_DEV'),
(3, 'MITS03', 'SYS_DEV'),
(4, 'MITS04', 'SYS_DEV'),
(5, 'MITS05', 'SYS_DEV'),
(6, 'MITS06', 'SYS_DEV'),
(7, 'MITS07', 'Info_Sys'),
(8, 'MITS08', 'Info_Sys'),
(9, 'MITS09', 'Info_Sys'),
(10, 'MITS10', 'Info_Sys'),
(11, 'MITS11', 'Info_Sys'),
(12, 'MITS12', 'Info_Sys'),
(23, 'MSCSS01', 'Apl_Cmp'),
(24, 'MSCSS02', 'Apl_Cmp'),
(25, 'MSCSS03', 'Apl_Cmp'),
(26, 'MSCSS04', 'Apl_Cmp'),
(27, 'MSCSS05', 'Apl_Cmp'),
(28, 'MSCSS06', 'Apl_Cmp'),
(30, 'MSCSS07', 'Grap_ComVis'),
(31, 'MSCSS08', 'Grap_ComVis'),
(32, 'MSCSS09', 'Grap_ComVis'),
(33, 'MSCSS10', 'Grap_ComVis'),
(40, 'MSCSS11', 'Grap_ComVis'),
(41, 'MSCSS12', 'Grap_ComVis'),
(42, 'MCGA201', 'Gen'),
(43, 'MCGA202', 'Gen'),
(44, 'MCGA203', 'Gen'),
(45, 'MCGA204', 'Gen'),
(46, 'MCGA205', 'Gen'),
(47, 'MCGA206', 'Gen'),
(48, 'MCGA211', 'SCGDDP'),
(49, 'MCGA212', 'SCGDDP'),
(50, 'MCGA213', 'SCGDDP'),
(51, 'MCGA214', 'SCGDDP'),
(52, 'MCGA215', 'SCGDDP'),
(53, 'MCGA216', 'SCGDDP'),
(56, 'MCGA221', 'DAP'),
(57, 'MCGA222', 'DAP'),
(58, 'MCGA223', 'DAP'),
(59, 'MCGA224', 'DAP'),
(60, 'MCGA225', 'DAP'),
(61, 'MCGA226', 'DAP');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `fee`
--
ALTER TABLE `fee`
  ADD PRIMARY KEY (`fee_code`);

--
-- Indexes for table `program`
--
ALTER TABLE `program`
  ADD PRIMARY KEY (`program_code`);

--
-- Indexes for table `subject`
--
ALTER TABLE `subject`
  ADD PRIMARY KEY (`subject_code`),
  ADD KEY `program` (`program_code`);

--
-- Indexes for table `track`
--
ALTER TABLE `track`
  ADD PRIMARY KEY (`track_code`),
  ADD KEY `program` (`program_code`);

--
-- Indexes for table `track_subject`
--
ALTER TABLE `track_subject`
  ADD PRIMARY KEY (`id`),
  ADD KEY `track_code` (`track_code`),
  ADD KEY `subject_code` (`subject_code`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `track_subject`
--
ALTER TABLE `track_subject`
  MODIFY `id` int(150) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=65;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `subject`
--
ALTER TABLE `subject`
  ADD CONSTRAINT `Subject_ibfk_1` FOREIGN KEY (`program_code`) REFERENCES `program` (`program_code`);

--
-- Constraints for table `track`
--
ALTER TABLE `track`
  ADD CONSTRAINT `Track_ibfk_1` FOREIGN KEY (`program_code`) REFERENCES `program` (`program_code`);

--
-- Constraints for table `track_subject`
--
ALTER TABLE `track_subject`
  ADD CONSTRAINT `track_subject_ibfk_1` FOREIGN KEY (`track_code`) REFERENCES `track` (`track_code`),
  ADD CONSTRAINT `track_subject_ibfk_2` FOREIGN KEY (`subject_code`) REFERENCES `subject` (`subject_code`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
