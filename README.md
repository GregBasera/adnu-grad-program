Authors:
  Basera
    Modules: Login, Home, Admission and Enrollment, General Policy

  Delas Alas, Abanes
    Modules: Programs, MySQL Database

  Rubio, Damasig
    Modules: Plan of Study, PDF generation


Enviroment:
  Web Elements Structure - HTML, CSS, Javascript
  Server-Side            - PHP in Codeigniter Framework
  Server                 - XAMPP [Default Config]
  Database               - MySQL
