
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Fees extends CI_Controller {

	public function index()
	{
		// Connect to the Database
		$this->load->database();

		// Get all the programs
		///$data["programs"] = $this->db->get('program');
		// Get all the tracks
		///$data["tracks"] = $this->db->get('track');
		// Get all the subjects
		///$data["subjects"] = $this->db->get('subject');
		// Get all track subjects
		///$this->db->select('*');
		///$this->db->from('subject');
		///$this->db->join('track_subject', 'subject.subject_code = track_subject.subject_code');
		///$query = $this->db->get();
		///$data["track_subjects"] = $query;

		// Load the plan of study page
		///$this->load->view('templates/header');
		///$this->load->view('templates/toolbar');
		$this->load->view('pages/printpdf.php', $data);
		///$this->load->view('templates/footer');
	}
	/*public function fees()
	{
		$this->load->database();
	}*/	
}
