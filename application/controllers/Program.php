
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Program extends CI_Controller {

	public function index()
	{
		// Connect to the Database
		$this->load->database();

		// Get all the Subject under the Core Module
        $this->db->select('*');
        $this->db->from('subject');
        $this->db->where("program_code",$this->input->get("id"));
        $this->db->where("subject_type","c");
        $data["c_subjects"] = $this->db->get();

        // Get the ID of the Program
        $data["id"] = $this->input->get("id");

        // Get the the name of the Program
        $this->db->select("*");
        $this->db->from("program");
        $this->db->where("program_code",$this->input->get("id"));
        $data["same"] = $this->db->get();

		// Get all the Subject under the Bridging Module
        $this->db->select('*');
        $this->db->from('subject');
        $this->db->where("program_code",$this->input->get("id"));
        $this->db->where("subject_type","brg");
        $data["b_subjects"] = $this->db->get();

		// Get all the Subject under the Thesis Module
        $this->db->select('*');
        $this->db->from('subject');
        $this->db->where("program_code",$this->input->get("id"));
        $this->db->where("subject_type","ths");
        $data["t_subjects"] = $this->db->get();

        // Get all the Subject under the Thesis Module
        $this->db->select('*');
        $this->db->from('subject');
        $this->db->where("program_code",$this->input->get("id"));
        $this->db->where("subject_type","ths");
        $data["t_subjects"] = $this->db->get();
        $sub = "subject";

        $data["tr_subjects"] = $this->db->query("SELECT * from ".$sub." s, track t, track_subject ts where s.subject_code  = ts.subject_code  and t.track_code = ts.track_code and s.program_code = '".$this->input->get("id")."' and subject_type = 'trk'");

        $this->db->select('*');
        $this->db->from('track');
        $this->db->where("program_code",$this->input->get("id"));
        $data["tracks"] = $this->db->get();

		//Load the plan of study page
		// $this->load->view('templates/header');
		// $this->load->view('templates/toolbar');
		// $this->load->view('pages/Programs_details.php',$data);
		// $this->load->view('templates/footer');
		$data["title"] = 'Programs';
		$this->load->model('LogonModel');
		$data['user'] = $this->LogonModel->getUser();

		$this->load->view('pages/prog_page', $data);
	}

}
