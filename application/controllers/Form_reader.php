<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Form_reader extends CI_Controller {

	public function index(){
		// Load the plan of study page
		$this->load->view('templates/header');
		$this->load->view('pages/study.php');
		$this->load->view('templates/footer');
	}

	public function save_userinput(){
		// Get the student information
		$lastname = $this->input->post('lastname');
		$firstname = $this->input->post('firstname');
		$mi = $this->input->post('mi');
		$degree = $this->input->post('degree');
		$track = $this->input->post('track');
		// Get the semester
		$semester_array = $this->input->post('select-semester');

		// Get the school year
		$school_year_array = $this->input->post('select-school-year');

		// Get the subjects
		$pdf_content = $this->input->post('loader_textarea');

		// Get the number of subjects
		$subjectCounter = $this->input->post('subject-counter');

		// Get the subject information
		$subjectCode = $this->input->post('subject-code');
		$subjectTitle = $this->input->post('subject-title');
		$subjectUnit = $this->input->post('subject-units');
		$totalUnits = $this->input->post('total-units');

		$this->load->database();

		$trackDB = $this->db->get('track');
		$tracks = [];
		foreach ($trackDB->result() as $value) {
			$tracks[$value->track_code] = $value->track_name;
		}

		$programsDB = $this->db->get('program');
		$degrees = [];
		foreach ($programsDB->result() as $value) {
			$degrees[$value->program_code] = $value->program_name;
		}

		$subjectsArray = [];
		// Get all subjects
		$subjects = $this->db->get('subject');

		foreach ($subjects->result() as $subject ) {
			foreach ($subjectCode as $code) {
				if($code == $subject->subject_code) {
					$subjectsArray[$subject->subject_code] = $subject;
				}
			}
		}

	// Prepare the data
    $data = array(
		'pdf_content' => $pdf_content,
		'school_year_array' => $school_year_array,
		'semester_array' => $semester_array,
		'lastname' => $lastname,
		'firstname' => $firstname,
		'mi' => $mi,
		'degree' => $degree,
		'track' => $track,
		'subjectCounter' => $subjectCounter,
		'subjectCode' => $subjectCode,
		'subjectTitle' => $subjectTitle,
		'subjectUnit' => $subjectUnit,
		'totalUnits' => $totalUnits,
		'degrees' => $degrees,
		'tracks' => $tracks,
		'subjects' => $subjectsArray,
    );

    // Load the plan of study page
    $this->load->view('templates/header_pdf');
    $this->load->view('pages/convert_to_pdf.php', $data);
	}

	public function save_programs(){
		// Connect to the Database
		$this->load->database();

		$code = $this->input->post('Pcode');
		$name = $this->input->post('names');
		$description = $this->input->post('desc');

		$this->db->set("Program_Code",$code);
		$this->db->set("Program_Name",$name);
		$this->db->set("Program_Details",$description);
		$this->db->insert('program');

		// header('Location: programs.php');
		// $this->load->helper('program');
		redirect('programs');
	}

	public function delete_programs(){
		echo $this->input->get('id');

		$this->load->database();
		$this->db->where("program_code", $this->input->get('id'));
		$this->db->delete("program");
		redirect('programs');

		// header('Location: programs.php');
	}

	public function delete_subject(){
		echo $this->input->get('id');
		echo $this->input->get('prog');
		$this->load->database();
		$this->db->where("Subject_Code",$this->input->get('id'));
		$this->db->delete("subject");

		redirect('program?id='.$this->input->get('prog'));
	}

	public function delete_subject_Tracks(){
		echo $this->input->get('id');
		echo $this->input->get('tr');

		$this->load->database();
		$this->db->where("Track_Code",$this->input->get('tr'));
		$this->db->where("Subject_Code",$this->input->get('id'));
		$this->db->delete("track_subject");

		$this->db->where("Subject_Code",$this->input->get('id'));
		$this->db->delete("subject");

		redirect('program?id='.$this->input->get('prog'));

		// redirect('program?id='.$this->input->get('prog'));
	}

	public function add_subjects(){
		$this->load->database();

		$code = $this->input->post('code');
		$name = $this->input->post('name');
		$units = $this->input->post('units');
		$Fee = $this->input->post('fee');
		$s_type = $this->input->post('S_Type');
		$program =$this->input->post('program');

		$this->db->set("Subject_Code",$code);
		$this->db->set("Subject_Name",$name);
		$this->db->set("Units",$units);
		$this->db->set("Fee_Type",$Fee);
		$this->db->set("Subject_Type",$s_type);
		$this->db->set("Program_Code",$program);
		$this->db->insert('Subject');
		redirect('program?id='.$program);

	}

	public function add_subjects_Tracks(){
		$this->load->database();

		$code = $this->input->post('code');
		$name = $this->input->post('name');
		$units = $this->input->post('units');
		$Fee = $this->input->post('fee');
		$s_type = $this->input->post('S_Type');
		$program =$this->input->post('program');
		$track =$this->input->post('track');

		$this->db->set("Subject_Code",$code);
		$this->db->set("Subject_Name",$name);
		$this->db->set("Units",$units);
		$this->db->set("Fee_Type",$Fee);
		$this->db->set("Subject_Type",$s_type);
		$this->db->set("Program_Code",$program);
		$this->db->insert('Subject');

		$this->db->set("Subject_Code",$code);
		$this->db->set("Track_Code",$track);
		$this->db->insert('Track_Subject');


		redirect('program?id='.$program);
	}

	public function AddTrack(){
		$this->load->database();
		$code = $this->input->post('code');
		$name = $this->input->post('name');
		$prog = $this->input->post('program');

		$this->db->set("Track_Code",$code);
		$this->db->set("Track_Name",$name);
		$this->db->set("Program",$prog);
		$this->db->insert('Track');

		// redirect('program?id='.$prog);

	}
}
