<?php
  defined('BASEPATH') OR exit('No direct script access allowed');

  class AdEn extends CI_Controller{
  	public function index(){
      $data['title'] = "Admission and Enrollment";

      $this->load->model('LogonModel');
      $data['user'] = $this->LogonModel->getUser();

      $this->load->view('pages\aden_page', $data);
    }

    function subjOffer(){
      $this->load->view('pages\redirect.html');
    }
  }
?>
