<?php
  defined('BASEPATH') OR exit('No direct script access allowed');

  class Home extends CI_Controller{
  	public function index(){
      $data['title'] = "Home";

      $this->load->model('LogonModel');
      $data['user'] = $this->LogonModel->getUser();

      $this->load->model('HomeModel');
      $data['announcements'] = $this->HomeModel->getAnnouncements();
      $data['announcementsAlt'] = $this->HomeModel->getAnnouncementsAlt();
      $data['downloadables'] = $this->HomeModel->getDownloadables();

  		$this->load->view('pages/home_page', $data);
  	}

    public function uploadDown(){
      $file = $_FILES['file'];
      $name = $_POST['name'];

      if(empty($file) || empty($name)){
        header("Location: ../Home.php?save=empty");
      }
      else{
        $fileName = $_FILES['file']['name'];
        $fileType = $_FILES['file']['type'];
        $fileSive = $_FILES['file']['size'];
        $fileErro = $_FILES['file']['error'];
        $fileTemp = $_FILES['file']['tmp_name'];

        $fileExt = explode('.', $fileName);
        $fileActualExt = strtolower(end($fileExt));

        $allowed = array('pdf');

        if(in_array($fileActualExt, $allowed)){
          if($fileErro === 0){
            $fileNameNew = $name.".".$fileActualExt;
            $fileDestination = 'assets/downloadable/'.$fileNameNew;
            move_uploaded_file($fileTemp, $fileDestination);
            // reload
            redirect('Home');
          }
        }
      }
    }
  }
?>
