
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Programs extends CI_Controller {

	public function index()
	{
		$data["title"] = 'Programs';
		$this->load->model('LogonModel');
		$data['user'] = $this->LogonModel->getUser();

		// Connect to the Database
		$this->load->database();

		// Get all the programs
    $data["programs"] = $this->db->get('program');

		// Load the plan of study page
		// $this->load->view('templates/head');
		// $this->load->view('templates/toolbar');
		// $this->load->view('pages/program.php', $data);
		// $this->load->view('templates/footer');

		$this->load->view('pages/pros_page', $data);
	}

}
