<?php
  defined('BASEPATH') OR exit('No direct script access allowed');

  class Login extends CI_Controller{
  	public function index(){
      $data['title'] = "ADNU CSS";

  		$this->load->view('pages/login_page', $data);
  	}

    public function validate(){
      $user = $this->input->post('username');
      $pass = $this->input->post('pass');

      if($user == 'admin' && $pass == 'admin'){
        $this->load->model('LogonModel');
        $this->LogonModel->setUser($user);
        redirect('Home');
      }
      else{
        redirect('Login');
      }
    }

    public function guest(){
      $this->load->model('LogonModel');
      $this->LogonModel->setUser('');
      redirect('Home');
    }
  }
?>
