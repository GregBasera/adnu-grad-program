<?php
  defined('BASEPATH') OR exit('No direct script access allowed');

  class GePo extends CI_Controller{
  	public function index(){
      $data['title'] = "General Policy";

      $this->load->model('LogonModel');
      $data['user'] = $this->LogonModel->getUser();

      $this->load->view('pages\gepo_page', $data);
    }
  }
?>
