<?php
  defined('BASEPATH') OR exit('No direct script access allowed');

  class HomeModel extends CI_Model{
    public function getAnnouncements(){
      $ann = file_get_contents('assets\Announcements.txt');
      $chopped = explode('#@#', $ann);
      return $chopped;
    }

    public function getAnnouncementsAlt(){
      $ann = file_get_contents('assets\Announcements.txt');
      $annAlt = str_replace('"', "~@", $ann);
      $annAlt = str_replace("'", "@~", $annAlt);
      $annAlt = preg_replace('~[\r\n]+~', '', $annAlt);
      $choppedAlt = explode('#@#', $annAlt);
      return $choppedAlt;
    }

    public function getDownloadables(){
      $files = scandir('assets\downloadable', 1);
      return $files;
    }
  }

?>
