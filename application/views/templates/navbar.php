<div class="navbar">
  <a class="nav-link nav-link-icon p-0 toggle-sidebar d-md-none d-lg-none" data-toggle="collapse" data-target=".header-navbar" aria-expanded="false" aria-controls="header-navbar">
    <i class="material-icons">&#xE5D2;</i>
  </a>
  <h5><?php echo $title ?></h5>
  <div class="btn-group">
    <button class="btn btn-secondary dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
      <i class="material-icons">person</i>
      <?php echo $user ?>
    </button>
    <div class="dropdown-menu dropdown-menu-right">
      <?php
        if($user != NULL){
          echo "<button class='dropdown-item' type='button'>Settings</button>";
          echo "<a href='Login'><button class='dropdown-item' type='button'>Log Out</button></a>";
        }
        else{
          echo "<a href='Login'><button class='dropdown-item' type='button'>Login</button></a>";
        }
      ?>
    </div>
  </div>
</div>
