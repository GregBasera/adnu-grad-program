<div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="modalcontents" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="modalcontents">Delete</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" id="deleteAnn">

      </div>
      <div class="modal-footer">
        <div class="m-2" id="spinner3">
          <?php include('assets\throbber_small.svg'); ?>
        </div>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" onclick="deleteAnnouncement()">Delete</button>
      </div>
    </div>
  </div>
</div>
