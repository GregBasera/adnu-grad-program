<div class="btn-group float-right">
  <div class="cursor dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
    <i class="material-icons">edit</i>
  </div>
  <div class="dropdown-menu dropdown-menu-right">
    <button class="dropdown-item" type="button" data-toggle="modal" data-target="#editModal">Edit</button>
    <!-- <button class="dropdown-item" type="button">Delete</button> -->
  </div>
</div>



<?php
  $source = "";
  if(strpos(getUrl(), "Admission_and_Enrollment") == true)
    $source = "AdEn";
  else
    $source = "GePo";
?>

<!-- Modal -->
<div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="modalcontents" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="modalcontents">Edit</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <?php kwillmodule(); ?>
      </div>
      <div class="modal-footer">
        <div class="m-2" id="spinner1">
          <?php include('assets\throbber_small.svg'); ?>
        </div>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" onclick="store('<?php echo $source; ?>')">Save changes</button>
      </div>
    </div>
  </div>
</div>
