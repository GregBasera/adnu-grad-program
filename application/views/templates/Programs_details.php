<div class="main-content-container container-fluid p-2">
    <!-- Page Header -->
    <div class="page-header row no-gutters p-2">
      <div class="col-12 col-sm-6 text-center text-sm-left mb-0">
        <span class="text-uppercase page-subtitle">Program:</span>
        <h3 class="page-title">
            <?php
                foreach ($same->result() as $prog):
                    echo $prog->program_name;
                endforeach
            ?>
            <small>(<?php print $id?>)</small>
        </h3>
      </div>
    </div>
    <div class="row">
        <!-- Student Information -->
        <div class="col-lg-12 col-md-12 col-sm-12 mb-4">
          <div class="card card-small">
            <div class="card-header border-bottom">
              <div class="row justify-content-between">
                <div class="col-4">
                  <h6 class="m-0">Core Subjects</h6>
                </div>
                <div class="col-4">
                  <?php
                    if($user != NULL){
                      echo '<button type="button" class="btn btn-sm btn-info float-right" data-toggle="modal" data-target="#cmodal"><i class="fa fa-plus mr-1"></i>Add Subject</button>';
                    }
                  ?>
                </div>
              </div>
            </div>

            <div class="card-body pt-0">
              <div class="row p-2 bg-light">
                <div class="col-sm-12">
                  <table class="table table-striped">
                    <thead>
                      <tr>
                        <th>Subject Code</th>
                        <th>Subject Name</th>
                        <th>Units</th>
                        <th>Room</th>
                        <?php
                          if($user != NULL){
                            echo "<th>Delete</th>";
                          }
                        ?>
                      </tr>
                    </thead>
                    <tbody>
                      <?php
                        foreach ($c_subjects->result() as $sub):
                          echo "<tr>
                            <td>".$sub->subject_code."</td>
                            <td>".$sub->subject_name."</td>
                            <td>".$sub->units."</td>
                            <td>".$sub->room."</td>";
                            if($user != NULL){
                              echo "<td><a href='form_reader/delete_subject?id=".$sub->subject_code."&prog=".$sub->program_code."' class='btn btn-danger'><i class='material-icons'>delete</i>Delete</a>";
                            }
                          echo "</tr>";
                        endforeach
                      ?>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
    </div>
    <div class="row">
        <!-- Student Information -->
        <div class="col-lg-12 col-md-12 col-sm-12 mb-4">
            <div class="card card-small">
                <div class="card-header border-bottom">
                    <div class="row justify-content-between">
                        <div class="col-4">
                            <h6 class="m-0">Elective Track:</h6>
                        </div>
                        <div class="col-4">
                          <?php
                            if($user != NULL){
                              echo '<button type="button" class="btn btn-sm btn-info float-right" data-toggle="modal" data-target="#amodal"><i class="fa fa-plus mr-1"></i>Add Track</button>';
                            }
                          ?>
                        </div>
                    </div>
                </div>
                <div class="card-body pt-0">
                    <div class="row p-2 bg-light">
                        <div class="col-sm-12">
                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <th>Subject Code</th>
                                        <th>Subject Name</th>
                                        <th>Units</th>
                                        <th>Track</th>
                                        <th>Room</th>
                                        <?php
                                          if($user != NULL)
                                            echo "<th>Delete</th>";
                                        ?>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                        foreach ($tr_subjects->result() as $sub):
                                            echo "<tr>
                                                <td>".$sub->subject_code."</td>
                                                <td>".$sub->subject_name."</td>
                                                <td>".$sub->units."</td>
                                                <td>".$sub->track_name."</td>
                                                <td>".$sub->room."</td>";
                                                if($user != NULL)
                                                  echo "<td><a href='form_reader/delete_subject_Tracks?id=".$sub->subject_code."&prog=".$sub->program_code."&tr=".$sub->track_code."' class='btn btn-danger'><i class='material-icons'>delete</i>Delete</a>";
                                            echo "</tr>";
                                        endforeach
                                    ?>
                                </tbody>
                            </table>
                        </div>
                        <div class="col-12">
                          <?php
                            if($user != NULL){
                              echo '<button type="button" class="btn btn-sm btn-default float-right" data-toggle="modal" data-target="#trkmodal"><i class="fa fa-plus mr-1"></i>Add Subject</button>';
                            }
                          ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <!-- Student Information -->
        <div class="col-lg-12 col-md-12 col-sm-12 mb-4">
            <div class="card card-small">
                <div class="card-header border-bottom">
                    <div class="row justify-content-between">
                        <div class="col-4">
                            <h6 class="m-0">Thesis Project</h6>
                        </div>
                        <div class="col-4">
                          <?php
                            if($user != NULL){
                              echo '<button type="button" class="btn btn-sm btn-info float-right" data-toggle="modal" data-target="#thsmodal">
                                  <i class="fa fa-plus mr-1"></i>Add Subject
                              </button>';
                            }
                          ?>
                        </div>
                    </div>
                </div>
                <div class="card-body pt-0">
                    <div class="row p-2 bg-light">
                        <div class="col-sm-12">
                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <th>Subject Code</th>
                                        <th>Subject Name</th>
                                        <th>Units</th>
                                        <th>Room</th>
                                        <?php
                                          if($user != NULL)
                                            echo "<th>Delete</th>";
                                        ?>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                        foreach ($t_subjects->result() as $sub):
                                            echo "<tr>
                                                <td>".$sub->subject_code."</td>
                                                <td>".$sub->subject_name."</td>
                                                <td>".$sub->units."</td>
                                                <td>".$sub->room."</td>";
                                                if($user != NULL)
                                                  echo "<td><a href='form_reader/delete_subject?id=".$sub->subject_code."&prog=".$sub->program_code."' class='btn btn-danger'><i class='material-icons'>delete</i>Delete</a>";
                                            echo "</tr>";
                                        endforeach
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php if ($id == "MIT"){?>
        <div class="row">
            <!-- Student Information -->
            <div class="col-lg-12 col-md-12 col-sm-12 mb-4">
                <div class="card card-small">
                    <div class="card-header border-bottom">
                        <div class="row justify-content-between">
                            <div class="col-4">
                                <h6 class="m-0">Comprehensive Examination</h6>
                            </div>
                        </div>
                    </div>
                    <div class="card-body pt-0">
                        <div class="row p-2 bg-light">
                            <div class="col-sm-12">
                                <h6>Upon completion of all the core and specialization courses, the student must take and pass the
    written comprehensive examinations to qualify for the <strong>Graduate Project </strong> courses.</h6>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <?php }?>
    <div class="row">
        <!-- Student Information -->
        <div class="col-lg-12 col-md-12 col-sm-12 mb-4">
            <div class="card card-small">
                <div class="card-header border-bottom">
                    <div class="row justify-content-between">
                        <div class="col-4">
                            <h6 class="m-0">Bridging Subjects</h6>
                        </div>
                        <div class="col-4">
                          <?php
                            if($user != NULL){
                              echo '<button type="button" class="btn btn-sm btn-info float-right" data-toggle="modal" data-target="#brgmodal">
                                  <i class="fa fa-plus mr-1"></i>Add Subject
                              </button>';
                            }
                          ?>
                        </div>
                    </div>
                </div>
                <div class="card-body pt-0">
                    <div class="row p-2 bg-light">
                        <div class="col-sm-12">
                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <th>Subject Code</th>
                                        <th>Subject Name</th>
                                        <th>Units</th>
                                        <th>Room</th>
                                        <?php
                                          if($user != NULL)
                                            echo "<th>Delete</th>";
                                        ?>
                                    </tr>
                                </thead>
                                    <?php
                                        foreach ($b_subjects->result() as $sub):
                                            echo "<tr>
                                                <td>".$sub->subject_code."</td>
                                                <td>".$sub->subject_name."</td>
                                                <td>".$sub->units."</td>
                                                <td>".$sub->room."</td>";
                                                if($user != NULL)
                                                echo "<td><a href='form_reader/delete_subject?id=".$sub->subject_code."&prog=".$sub->program_code."' class='btn btn-danger'><i class='material-icons'>delete</i>Delete</a>";
                                            echo "</tr>";
                                        endforeach
                                    ?>
                                <tbody>

                                    <!-- <tr>
                                        <td>John</td>
                                        <td>Doe</td>
                                        <td>john@example.com</td>
                                    </tr> -->
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <!-- Trigger the modal with a button -->
    <!-- <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">Open Modal</button> -->

    <!-- Modal -->
    <div class="modal fade" id="cmodal" role="dialog">
        <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Add Subject</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <form action="form_reader/add_subjects" method="post">
            <div class="modal-body">
                <div class="form-group">
                    <label for="code">Subject Code:</label>
                    <input type="text" class="form-control" name="code">
                </div>
                <div class="form-group">
                    <label for="name">Subject Name:</label>
                    <input type="text" class="form-control" name="name">
                </div>
                <div class="form-group">
                    <label for="desc">Units:</label>
                    <input type="text" class="form-control" name="units">
                </div>
                <div class="form-group">
                    <label for="sel1">Fee Type:</label>
                    <select class="form-control" id="sel1" name="fee">
                        <option value="None">None</option>
                        <option value="EDP Laboratory Fee">EDP Laboratory Fee</option>
                        <option value="Energy_Fee">Energy_Fee</option>
                        <option value="DIA_Laboratory Fee">DIA_Laboratory Fee</option>
                    </select>
                </div>
                <div class="form-group">
                    <label for="name">Subject Type:</label>
                    <!-- <input type="text" class="form-control" name="name"> -->
                    <p> Core</p>
                </div>
                <input type="hidden" name="S_Type" value="c">
                <input type="hidden" name="program" value="<?php echo $id; ?>">


                </div>
                <div class="modal-footer">
                    <input type="submit" value="Submit" class="btn btn-success">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </form>

        </div>
    </div>

</div>


<div class="container">
    <!-- Trigger the modal with a button -->
    <!-- <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">Open Modal</button> -->

    <!-- Modal -->
    <div class="modal fade" id="brgmodal" role="dialog">
        <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Add Subject</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <form action="form_reader/add_subjects" method="post">
            <div class="modal-body">
                <div class="form-group">
                    <label for="code">Subject Code:</label>
                    <input type="text" class="form-control" name="code">
                </div>
                <div class="form-group">
                    <label for="name">Subject Name:</label>
                    <input type="text" class="form-control" name="name">
                </div>
                <div class="form-group">
                    <label for="desc">Units:</label>
                    <input type="text" class="form-control" name="units">
                </div>
                <div class="form-group">
                    <label for="sel1">Fee Type:</label>
                    <select class="form-control" id="sel1" name="fee">
                        <option value="None">None</option>
                        <option value="EDP Laboratory Fee">EDP Laboratory Fee</option>
                        <option value="Energy_Fee">Energy_Fee</option>
                        <option value="DIA_Laboratory Fee">DIA_Laboratory Fee</option>
                    </select>
                </div>
                <div class="form-group">
                    <label for="name">Subject Type:</label>
                    <!-- <input type="text" class="form-control" name="name"> -->
                    <p> Bridging</p>
                </div>
                <input type="hidden" name="S_Type" value="brg">
                <input type="hidden" name="program" value="<?php echo $id; ?>">


                </div>
                <div class="modal-footer">
                    <input type="submit" value="Submit" class="btn btn-success">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </form>

        </div>
    </div>

</div>
<div class="container">
					<!-- Trigger the modal with a button -->
					<!-- <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">Open Modal</button> -->

					<!-- Modal -->
					<div class="modal fade" id="amodal" role="dialog">
					  <div class="modal-dialog">

						<!-- Modal content-->
						<div class="modal-content">
						  <div class="modal-header">
							  <h4 class="modal-title">Add Track</h4>
							  <button type="button" class="close" data-dismiss="modal">&times;</button>
						  </div>
						  <form action="form_reader/AddTrack" method="post">
							<div class="modal-body">
								<div class="form-group">
									<label for="code">Track Code:</label>
									<input type="text" class="form-control" name="code">
								</div>
								<div class="form-group">
									<label for="name">Track Name:</label>
									<input type="text" class="form-control" name="name">
								<input type="hidden" name="program" value="<?php echo $id; ?>">

								</div>
								</div>
								<div class="modal-footer">
									<input type="submit" value="Submit" class="btn btn-success">
									<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
								</div>
							</div>
						</form>

					  </div>
					</div>

				</div>
                <div class="container">
					<!-- Trigger the modal with a button -->
					<!-- <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">Open Modal</button> -->

					<!-- Modal -->
					<div class="modal fade" id="thsmodal" role="dialog">
					  <div class="modal-dialog">

						<!-- Modal content-->
						<div class="modal-content">
						  <div class="modal-header">
							  <h4 class="modal-title">Add Subject</h4>
							  <button type="button" class="close" data-dismiss="modal">&times;</button>
						  </div>
						  <form action="form_reader/add_subjects" method="post">
							<div class="modal-body">
								<div class="form-group">
									<label for="code">Subject Code:</label>
									<input type="text" class="form-control" name="code">
								</div>
								<div class="form-group">
									<label for="name">Subject Name:</label>
									<input type="text" class="form-control" name="name">
								</div>
								<div class="form-group">
									<label for="desc">Units:</label>
									<input type="text" class="form-control" name="units">
								</div>
								<div class="form-group">
									<label for="sel1">Fee Type:</label>
									<select class="form-control" id="sel1" name="fee">
										<option value="None">None</option>
										<option value="EDP Laboratory Fee">EDP Laboratory Fee</option>
										<option value="Energy_Fee">Energy_Fee</option>
										<option value="DIA_Laboratory Fee">DIA_Laboratory Fee</option>
									</select>
								</div>
								<div class="form-group">
									<label for="name">Subject Type:</label>
									<!-- <input type="text" class="form-control" name="name"> -->
									<p> Thesis Project</p>
								</div>
								<input type="hidden" name="S_Type" value="ths">
								<input type="hidden" name="program" value="<?php echo $id; ?>">


								</div>
								<div class="modal-footer">
									<input type="submit" value="Submit" class="btn btn-success">
									<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
								</div>
							</div>
						</form>

					  </div>
					</div>

                </div>
                <div class="container">
					<!-- Trigger the modal with a button -->
					<!-- <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">Open Modal</button> -->

					<!-- Modal -->
					<div class="modal fade" id="trkmodal" role="dialog">
					  <div class="modal-dialog">

						<!-- Modal content-->
						<div class="modal-content">
						  <div class="modal-header">
							  <h4 class="modal-title">Add Subject</h4>
							  <button type="button" class="close" data-dismiss="modal">&times;</button>
						  </div>
						  <form action="form_reader/add_subjects_Tracks" method="post">
							<div class="modal-body">
								<div class="form-group">
									<label for="code">Subject Code:</label>
									<input type="text" class="form-control" name="code">
								</div>
								<div class="form-group">
									<label for="name">Subject Name:</label>
									<input type="text" class="form-control" name="name">
								</div>
								<div class="form-group">
									<label for="desc">Units:</label>
									<input type="text" class="form-control" name="units">
								</div>
								<div class="form-group">
									<label for="sel1">Fee Type:</label>
									<select class="form-control" id="sel1" name="fee">
										<option value="None">None</option>
										<option value="EDP Laboratory Fee">EDP Laboratory Fee</option>
										<option value="Energy_Fee">Energy_Fee</option>
										<option value="DIA_Laboratory Fee">DIA_Laboratory Fee</option>
									</select>
                                </div>
                                <?php
                                echo"<div class='form-group'>";
                                echo"<label for='sel1'>Track Course:</label>";
									echo"<select class='form-control' id='sel1' name='track'>";
                                    echo"<option value='None'>None</option>";
                                            foreach ($tracks->result() as $t):
                                                echo" <option value=".$t->track_code.">".$t->track_code."</option>";
                                                // echo $t->Track_Code;
                                            endforeach;
                                            echo"</select>";
                                        echo"</div>";
                                ?>
								<div class="form-group">
									<label for="name">Subject Type:</label>
									<!-- <input type="text" class="form-control" name="name"> -->
									<p>Elective</p>
								</div>
								<input type="hidden" name="S_Type" value="trk">
								<input type="hidden" name="program" value="<?php echo $id; ?>">


								</div>
								<div class="modal-footer">
									<input type="submit" value="Submit" class="btn btn-success">
									<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
								</div>
							</div>
						</form>

					  </div>
					</div>

                </div>
