<div class="card">
  <div class="card-body">
    <h4 class="card-title border-bottom">Downloads</h4>
      <?php
        // $files = scandir('downloadable/', 1);

        for($q = 0; $q < count($downloadables)-2; $q++){
          if($user != NULL){
            echo "<a class='material-icons cursor-point float-right p-2' data-toggle='modal' data-target='#confirmDelete$q'>delete</a>";
            confirmDelete($downloadables[$q], $q);
          }
          echo "<a class='downloadables p-2' onclick='newtabview(\"$downloadables[$q]\")'>$downloadables[$q]</a>";
          echo "<div class='spacer w-100'></div>";
        }
        if($user != NULL)
          include('application\views\templates\add-button-downloads.php');
      ?>
  </div>
</div>
