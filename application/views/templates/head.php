<meta charset="utf-8">
<meta http-equiv="x-ua-compatible" content="ie=edge">
<link rel="icon" type="image/png" href="<?php echo base_url() ?>assets\img\logo.png">
<title>AdNU CCS GP</title>
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<!-- stylesheets -->
<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets\bootstrap\css\bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets\styles\shards-dashboards.1.0.0.min.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets\styles\extras.1.0.0.min.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets\quill\quill.core.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets\quill\quill.snow.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets\gweg\gweg.css">
<!-- scripts -->
<script type="text/javascript" src="<?php echo base_url() ?>assets\gweg\jquery-3.3.1.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/Sharrre/2.0.1/jquery.sharrre.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets\scripts\Chart.min.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets\bootstrap\js\bootstrap.min.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets\scripts\shards-dashboards.1.0.0.min.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets\scripts\extras.1.0.0.min.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets\quill\quill.min.js"></script>
<!-- php -->
<?php include('assets\gweg\gweg.php'); ?>
