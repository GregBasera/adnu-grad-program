<div class="container-fluid">
	<div class="row">
		<!-- Main Sidebar -->
		<aside class="main-sidebar col-12 col-md-3 col-lg-2 px-0">
			<div class="main-navbar">
				<nav class="navbar align-items-stretch navbar-light bg-white flex-md-nowrap border-bottom p-0">
					<a class="navbar-brand w-100 mr-0" href="#" style="line-height: 25px;">
						<div class="d-table m-auto">
							<span class="d-none d-md-inline ml-1">AdNU CCS</span>
						</div>
					</a>
					<a class="toggle-sidebar d-sm-inline d-md-none d-lg-none">
						<i class="material-icons">&#xE5C4;</i>
					</a>
				</nav>
				<div class="col-sm-12 p-4">
					<img class="img-fluid mx-auto" src="<?php echo base_url(); ?>static/img/logo.png" alt="">
				</div>
			</div>
			<div class="nav-wrapper">
				<ul class="nav flex-column">
					<li class="nav-item">
						<a class="nav-link" href="index.html">
							<i class="material-icons">home</i>
							<span>Home</span>
						</a>
					</li>
					<li class="nav-item">
						<a class="nav-link active" href="components-blog-posts.html">
							<i class="material-icons">vertical_split</i>
							<span>Plan of Study</span>
						</a>
					</li>
					<li class="nav-item">
						<a class="nav-link " href="add-new-post.html">
							<i class="material-icons">note_add</i>
							<span>Subject Offerings</span>
						</a>
					</li>
					<li class="nav-item">
						<a class="nav-link " href="form-components.html">
							<i class="material-icons">payment</i>
							<span>Payments</span>
						</a>
					</li>
				</ul>
			</div>
		</aside>
		<!-- End Main Sidebar -->
		<main class="main-content col-lg-10 col-md-9 col-sm-12 p-0 offset-lg-2 offset-md-3">

			<!-- Navbar -->
			<div class="main-navbar sticky-top bg-white">
				<!-- Main Navbar -->
				<nav class="navbar align-items-stretch navbar-light flex-md-nowrap p-0">
					<form action="#" class="main-navbar__search w-100 d-none d-md-flex d-lg-flex">

					</form>
					<ul class="navbar-nav border-left flex-row ">
						<li class="nav-item border-right dropdown notifications">
							<a class="nav-link nav-link-icon text-center" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true"
							aria-expanded="false">
								<div class="nav-link-icon__wrapper">
									<i class="material-icons">&#xE7F4;</i>
									<span class="badge badge-pill badge-danger">2</span>
								</div>
							</a>
							<div class="dropdown-menu dropdown-menu-small" aria-labelledby="dropdownMenuLink">
								<a class="dropdown-item" href="#">
									<div class="notification__icon-wrapper">
										<div class="notification__icon">
											<i class="material-icons">&#xE6E1;</i>
										</div>
									</div>
									<div class="notification__content">
										<span class="notification__category">Analytics</span>
										<p>Your website’s active users count increased by
											<span class="text-success text-semibold">28%</span> in the last week. Great job!</p>
									</div>
								</a>
								<a class="dropdown-item" href="#">
									<div class="notification__icon-wrapper">
										<div class="notification__icon">
											<i class="material-icons">&#xE8D1;</i>
										</div>
									</div>
									<div class="notification__content">
										<span class="notification__category">Sales</span>
										<p>Last week your store’s sales count decreased by
											<span class="text-danger text-semibold">5.52%</span>. It could have been worse!</p>
									</div>
								</a>
								<a class="dropdown-item notification__all text-center" href="#"> View all Notifications </a>
							</div>
						</li>
						<li class="nav-item dropdown">
							<a class="nav-link dropdown-toggle text-nowrap px-3" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
								<img class="user-avatar rounded-circle mr-2" src="<?php echo base_url(); ?>static/images/avatars/0.jpg" alt="User Avatar">
								<span class="d-none d-md-inline-block">Sierra Brooks</span>
							</a>
							<div class="dropdown-menu dropdown-menu-small">
								<a class="dropdown-item" href="user-profile-lite.html">
									<i class="material-icons">&#xE7FD;</i> Profile</a>
								<a class="dropdown-item" href="components-blog-posts.html">
									<i class="material-icons">vertical_split</i> Blog Posts</a>
								<a class="dropdown-item" href="add-new-post.html">
									<i class="material-icons">note_add</i> Add New Post</a>
								<div class="dropdown-divider"></div>
								<a class="dropdown-item text-danger" href="#">
									<i class="material-icons text-danger">&#xE879;</i> Logout </a>
							</div>
						</li>
					</ul>
					<nav class="nav">
						<a href="#" class="nav-link nav-link-icon toggle-sidebar d-md-inline d-lg-none text-center border-left" data-toggle="collapse"
						data-target=".header-navbar" aria-expanded="false" aria-controls="header-navbar">
							<i class="material-icons">&#xE5D2;</i>
						</a>
					</nav>
				</nav>
			</div>
			<!-- End Navber -->
