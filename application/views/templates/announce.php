<?php
  // $ann = file_get_contents('assets\Announcements.txt');
  // $chopped = explode('#@#', $ann);
  //
  // $annAlt = str_replace('"', "~@", $ann);
  // $annAlt = str_replace("'", "@~", $annAlt);
  // $annAlt = preg_replace('~[\r\n]+~', '', $annAlt);
  // $choppedAlt = explode('#@#', $annAlt);

  for($q = 1; $q < sizeof($announcements); $q++){
    echo "<div class='jumbotron shadow'>";
    if($user != NULL){
      echo "
        <div class='btn-group float-right'>
          <div class='cursor dropdown-toggle' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>
            <i class='material-icons'>edit</i>
          </div>
          <div class='dropdown-menu dropdown-menu-right'>
            <button class='dropdown-item' type='button' data-toggle='modal' data-target='#editModal' onclick='editModal(\"$announcementsAlt[$q]\", $q)'>Edit</button>
            <button class='dropdown-item' type='button' data-toggle='modal' data-target='#deleteModal' onclick='delAnnounce($q)'>Delete</button>
          </div>
        </div>";
    }
      echo "<div class='ql-container ql-editor'>".$announcements[$q]."</div>";
    echo "</div>";
  }

  if($user != NULL){
    include('application\views\templates\announce-edit-modal.php');
    include('application\views\templates\announce-delete-modal.php');
  }
?>
