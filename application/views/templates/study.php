<!-- Convert to PDF -->
<form name="userinput" action="form_reader/save_userinput" method="post" target="_blank">
	<!-- Enlistment Forms -->
	<div id="dynamicWrapper" class="main-content-container container-fluid p-2">

		<div class="row">
			<!-- Student Information -->
			<div class="col-lg-12 col-md-12 col-sm-12 mb-4">
				<div class="card card-small">
					<div class="card-header border-bottom">
						<div class="row justify-content-between">
							<h6 class="m-0">Student Information</h6>
							<div class="col-sm-8">
								<button name="create_pdf" id="create-pdf" type="submit" class="btn btn-sm btn-success float-right m-1" disabled>
									<i class="material-icons">arrow_downward</i> Download PDF
								</button>

								<!-- Hidden Text Area for PDF Conversion -->
								<textarea class="d-none" name="loader_textarea" id="loader_textarea" cols="125" rows="10"></textarea>
							</div>
						</div>
					</div>
					<div class="card-body pt-0">
						<div class="row p-2 bg-light">
							<div class="col-sm-6">
								<strong class="text-muted d-block mb-2">Full Name</strong>
								<div class="form-row">
									<div class="form-group col-sm-5">
										<input name="firstname" type="text" class="form-control" id="validationServer01" placeholder="First name" required="">
									</div>
									<div class="form-group col-sm-2">
										<input name="mi" type="text" class="form-control" id="validationServer02" placeholder="MI" required="">
									</div>
									<div class="form-group col-sm-5">
										<input name="lastname" type="text" class="form-control" id="validationServer02" placeholder="Last name"required="">
									</div>
								</div>
							</div>
							<div class="col-sm-3">
								<strong class="text-muted d-block mb-2">Degree</strong>
								<div class="form-row">
									<div class="form-group col-sm-12">
										<select id="select-degree" name="degree" class="form-control" required="true">
											<option selected="">Choose...</option>
											<!-- Retrieve all Programs from DB -->
											<?php
												foreach ($programs->result() as $program) {
													echo "<option>$program->program_code</option>";
												}
											?>
										</select>
									</div>
								</div>
							</div>
							<div class="col-sm-3">
								<strong class="text-muted d-block mb-2">Track</strong>
								<div class="form-row">
									<div class="form-group col-sm-12">
										<select id="select-track" name="track" class="form-control">
											<option selected="">Choose a degree first...</option>
											<!-- Retrieve all Tracks from DB -->
											<?php
												foreach ($tracks->result() as $track) {
													echo "<option value='$track->track_code' class='d-none track-$track->program_code'>$track->track_name</option>";
												}
											?>
										</select>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- End Student Information -->
		</div>
		<div id="pdf_content">
			<!-- First Enlistment Form Table -->
			<div class="row">
				<!-- Student Information -->
				<div id="x" class="outer-div col-lg-12 col-md-12 col-sm-12 mb-4">
					<div class="card card-small">
						<div class="card-header border-bottom">
							<h6 class="m-0">Semestral Plan of Study</h6>
						</div>
						<div class="card-body pt-0">
							<div class="row p-2 bg-light justify-content-start">
								<div class="col-3">
									<strong class="text-muted d-block mb-2">School Year</strong>
									<div class="form-row">
										<div class="form-group col-sm-12">
											<!-- Select school year -->
											<select id="first-select-school-year" name="select-school-year[]" class="form-control">
												<option selected="">Choose...</option>
												<option>2018 - 2019</option>
												<option>2019 - 2020</option>
												<option>2020 - 2021</option>
												<option>2021 - 2022</option>
												<option>2022 - 2023</option>
												<option>2023 - 2024</option>
												<option>2024 - 2025</option>
											</select>
										</div>
									</div>
								</div>
								<div class="col-3">
									<strong class="text-muted d-block mb-2">Semester</strong>
									<div class="form-row">
										<div class="form-group col-sm-12">
											<select name="select-semester[]" class="form-control select-semester">
												<option>First</option>
												<option>Second</option>
												<option>Summer</option>
											</select>
										</div>
									</div>
								</div>
							</div>

							<div class="row p-2 bg-light">
								<div class="col-sm-12">
									<table class="table mb-0 subject_table">
										<thead class="bg-light">
											<tr>
												<th scope="col" class="border-0">Subject Code</th>
												<th scope="col" class="border-0">Subject Title</th>
												<th scope="col" class="border-0">Units</th>
												<th scope="col" class="border-0 action-column">Actions</th>
											</tr>
										</thead>
										<tbody>
											<tr class="d-none">
												<td>
													<!-- Hidden input that counts the number of subjects -->
													<input type="text" class="subject-counter" value="0" name="subject-counter[]">
												</td>
											</tr>
											<!-- Total Units Row -->
											<tr class="total-units-row">
												<th colspan="2" class="text-right">Total</th>
												<th colspan="2" class="total-units-col">0</th>
											</tr>
											<tr>
												<th class="d-none"><input class='hidden-input-total-units' type="text" name='total-units[]'></th>
											</tr>
											<!-- Add Another Subject Row -->
											<tr class="add-subject-row">
												<td colspan="4" class="pt-3 text-left">
													<a href="#" class="add-subject cancelDefault" data-toggle="modal" data-target="#addSubjectModal">
														<i class="material-icons">add</i>Add Subject
													</a>
												</td>
											</tr>
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- End Student Information -->
			</div>

			<!-- Hidden Enlistment Form Template -->
			<div class="row d-none outer-div" id="hiddenEnlistmentForm">
				<!-- Student Information -->
				<div id="x" class="col-lg-12 col-md-12 col-sm-12 mb-4">
					<div class="card card-small">
						<div class="card-header border-bottom">
							<h6 class="m-0">Semestral Plan of Study</h6>
						</div>
						<div class="card-body pt-0">
							<div class="row p-2 bg-light justify-content-start">
								<div class="col-3">
									<strong class="text-muted d-block mb-2">School Year</strong>
									<div class="form-row">
										<div class="form-group col-sm-12">
											<select name="select-school-year[]" class="sub-select-school-year form-control">
												<option selected="">Choose...</option>
												<option>2018 - 2019</option>
												<option>2019 - 2020</option>
												<option>2020 - 2021</option>
												<option>2021 - 2022</option>
											</select>
										</div>
									</div>
								</div>
								<div class="col-3">
									<strong class="text-muted d-block mb-2">Semester</strong>
									<div class="form-row">
										<div class="form-group col-sm-12">
											<select name="select-semester[]" class="select-semester form-control">
												<option>First</option>
												<option>Second</option>
												<option>Summer</option>
											</select>
										</div>
									</div>
								</div>
							</div>

							<!-- First Enlistment Form -->
							<div class="row p-2 bg-light">
								<div class="col-sm-12">
									<table class="table mb-0 subject_table">
										<thead class="bg-light">
											<tr>
												<th scope="col" class="border-0">Subject Code</th>
												<th scope="col" class="border-0">Subject Title</th>
												<th scope="col" class="border-0">Units</th>
												<th scope="col" class="border-0 action-column">Actions</th>
											</tr>
										</thead>
										<tbody>
											<tr class="d-none">
												<td>
													<!-- Hidden input that counts the number of subjects -->
													<input type="text" class="subject-counter" value="0" name="subject-counter[]">
												</td>
											</tr>
											<!-- Total Units Row -->
											<tr class="total-units-row">
												<th colspan="2" class="text-right">Total</th>
												<th colspan="2" class="total-units-col">0</th>
											</tr>
											<tr>
												<th class="d-none"><input class='hidden-input-total-units' type="text" name='total-units[]'></th>
											</tr>
											<!-- Add Another Subject Row -->
											<tr class="add-subject-row">
												<td colspan="4" class="pt-3 text-left">
													<a href="#" class="add-subject cancelDefault" data-toggle="modal" data-target="#addSubjectModal">
														<i class="material-icons">add</i>Add Subject
													</a>
												</td>
											</tr>
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- End Student Information -->
			</div>
		</div>
	</div>
</form>

<!-- Add Semester Button -->
<div class="container">
	<div class="row pt-2 pb-3">
		<div class="col-sm-12 text-center">
			<a href="#" class="addSemester cancelDefault">
				<i class="material-icons">add</i>Add Semester
			</a>
		</div>
	</div>
</div>

<!-- Add Subject Modal -->
<div class="modal fade" id="addSubjectModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Subject Offerings</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<!-- Seamless Left Icon -->
				<div class="input-group input-group-seamless">
					<div class="input-group-prepend">
						<div class="input-group-text">
							<i class="material-icons">search</i>
						</div>
					</div>
					<input id="searchBar" type="text" class="form-control" aria-label="Text input with checkbox" placeholder="Search for subjects here">
				</div>
				<table class="table mb-0" id="modal-table">
					<thead class="bg-light">
						<tr id="subject-modal-header">
							<th scope="col" class="border-0">Subject Code</th>
							<th scope="col" class="border-0">Subject Title</th>
							<th scope="col" class="border-0">Units</th>
							<th scope="col" class="border-0">Actions</th>
						</tr>
					</thead>
					<tbody id="addSubjectModalBody">
						<tr>
							<td colspan="4" class="text-center" id="modal-degree-warning">
								<h6>Please choose a degree first</h6>
							</td>
						</tr>
						<!-- Retrieve all subjects from the Database -->
						<?php foreach ($subjects->result() as $subject): ?>
						<!-- Get all the non-TRACK subjects -->
						<?php if ($subject->subject_type != 'trk'): ?>
						<tr class="d-none subject-modal-row-<?php echo $subject->program_code;?>">
							<td>
								<?php echo $subject->subject_code ?>
							</td>
							<td>
								<?php echo $subject->subject_name ?>
							</td>
							<td>
								<?php echo $subject->units ?>
							</td>
							<td>
								<button id="<?php echo $subject->subject_code;?>" class="btn btn-sm btn-success add">
									<i class="material-icons">add</i>
									Add
								</button>
							</td>
						</tr>
						<?php endif; ?>
						<?php endforeach; ?>
						<!-- Get all the TRACK subjects -->
						<?php foreach ($track_subjects->result() as $subject): ?>
						<tr class="d-none track-subject track-subject-row-<?php echo $subject->track_code; ?> subject-modal-row-<?php echo $subject->program_code;?>">
							<td>
								<?php echo $subject->subject_code ?>
							</td>
							<td>
								<?php echo $subject->subject_name ?>
							</td>
							<td>
								<?php echo $subject->units ?>
							</td>
							<td>
								<button id="<?php echo $subject->subject_code;?>" class="btn btn-sm btn-success add">
									<i class="material-icons">add</i>
									Add
								</button>
							</td>
						</tr>
						<?php endforeach; ?>
					</tbody>
				</table>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>

<!-- Refresh Warning Modal -->
<div class="modal fade" id="refresh-warning-modal" tabindex="-1" role="dialog" aria-labelledby="refresh-warning-modal"
    aria-hidden="true">
	<div class="modal-dialog modal-sm" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Warning!</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<p>
					Changing your degree will drop all the subjects you have added.
				</p>
			</div>
			<div class="modal-footer justify-content-center">
				<button type="button" class="btn btn-sm btn-secondary" data-dismiss="modal">Cancel</button>
				<button id="refresh-continue" type="button" class="btn btn-sm btn-danger">Continue</button>
			</div>
		</div>
	</div>
</div>

<!-- Load Custom Scripts -->
<script src="<?php echo base_url(); ?>static/scripts/study.js"></script>
