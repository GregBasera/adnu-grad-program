<div class="card shadow mb-3">
  <div class="card-header border-bottom">
    <h5>Subject Offerings</h5>
  </div>
  <div class="card-body">
    <p class="card-text">See Offered Subjects SY: 2018-2019</p>
    <form action="<?php  echo base_url(); ?>AdEn/subjOffer">
      <button class="btn btn-primary btn-block" type="submit">Click me</button>
    </form>
  </div>
</div>

<div class="card shadow mb-3">
  <div class="card-header border-bottom">
    <h5>Enrollment Procedure</h5>
  </div>
  <div class="card-body list-group">
    <p class="card-text">See Enrollment Procedure: SY: 2018-2019</p>
    <form action="<?php  echo base_url(); ?>AdEn/subjOffer">
      <button class="btn btn-primary btn-block" type="submit">Click me</button>
    </form>
  </div>
</div>
