<div class="container-fluid">
	<div class="row">
		<!-- Main Sidebar -->
		<a class="toggle-sidebar d-sm-inline d-md-none d-lg-none border-0">
		  <i class="material-icons">&#xE5C4;</i>
		</a>
		<div class="container-fluid text-center my-3">
		  <h6><b>AdNU Graduate Program</b></h6>
		</div>

		<div class="container-fluid pt-2 pb-5 border-bottom">
		  <img src="assets\img\logo.png" alt="ADNU logo" height=144px width=144px>
		</div>

		<div class="nav-wrapper">
		  <ul class="nav flex-column">
		    <li class="nav-item">
		      <a class="nav-link " id="Home" href="Home">
		      <i class="material-icons">home</i>
		      <span>Home</span>
		      </a>
		    </li>
		    <li class="nav-item">
		      <a class="nav-link " id="Admission_and_Enrollment" href="Admission_and_Enrollment">
		      <i class="material-icons">vertical_split</i>
		      <span>Admission and Enrollment</span>
		      </a>
		    </li>
		    <li class="nav-item">
		      <a class="nav-link " id="General_Policy" href="General_Policy">
		      <i class="material-icons">content_paste</i>
		      <span>General Policy</span>
		      </a>
		    </li>
		    <li class="nav-item">
		      <a class="nav-link " href="Programs">
		      <i class="material-icons">library_books</i>
		      <span>Programs</span>
		      </a>
		    </li>
		    <li class="nav-item">
		      <a class="nav-link " href="Plan_of_Study">
		      <i class="material-icons">school</i>
		      <span>Plan of Study</span>
		      </a>
		    </li>
		  </ul>
		</div>

		<!-- End Main Sidebar -->
		<main class="main-content col-lg-10 col-md-9 col-sm-12 p-0 offset-lg-2 offset-md-3">

			<!-- Navbar -->
			<div class="main-navbar sticky-top bg-white">
				<!-- Main Navbar -->
				<nav class="navbar align-items-stretch navbar-light flex-md-nowrap p-0">
					<form action="#" class="main-navbar__search w-100 d-none d-md-flex d-lg-flex">

					</form>
					<ul class="navbar-nav border-left flex-row ">
						<a class="nav-link px-3 my-auto" href="#" role="button">
							Login
						</a>
					</ul>
					<nav class="nav">
						<a href="#" class="nav-link nav-link-icon toggle-sidebar d-md-inline d-lg-none text-center border-left" data-toggle="collapse"
						data-target=".header-navbar" aria-expanded="false" aria-controls="header-navbar">
							<i class="material-icons">&#xE5D2;</i>
						</a>
					</nav>
				</nav>
			</div>
			<!-- End Navber -->
