<button class="btn btn-outline-primary btn-block btn-sm mb-3" type="button" data-toggle="modal" data-target="#announcementAddModal">
  <i class="material-icons">add</i>Add
</button>





<!-- Modal -->
<div class="modal fade" id="announcementAddModal" tabindex="-1" role="dialog" aria-labelledby="modalcontents" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="modalcontents">Add</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div id="add-ql"></div>
      </div>
      <div class="modal-footer">
        <div class="m-2" id="spinner2">
          <?php include('assets\throbber_small.svg'); ?>
        </div>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" onclick="createAnnouncement()">Create</button>
      </div>
    </div>
  </div>
</div>
