<button class="btn btn-outline-primary btn-block btn-sm mb-3" type="button" data-toggle="modal" data-target="#downloadAddModal">
  <i class="material-icons">add</i>Add
</button>





<!-- Modal -->
<div class="modal fade" id="downloadAddModal" tabindex="-1" role="dialog" aria-labelledby="modalcontents" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="modalcontents">Upload</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action='Home/uploadDown' method='POST' enctype='multipart/form-data'>
        <div class="modal-body">
          <div class='form-group m-0'>
            <input class='form-control' type='file' name='file' required>
            <input class='form-control mt-3' type='text' name='name' placeholder='Upload As' required>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary" name="save">Upload</button>
        </div>
      </form>
    </div>
  </div>
</div>
