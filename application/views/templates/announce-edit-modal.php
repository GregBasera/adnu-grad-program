<div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="modalcontents" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="modalcontents">Edit</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <?php kwillmodule(); ?>
      </div>
      <div class="modal-footer">
        <div class="m-2" id="spinner1">
          <?php include('assets\throbber_small.svg'); ?>
        </div>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" onclick="store('home')">Save changes</button>
      </div>
    </div>
  </div>
</div>
