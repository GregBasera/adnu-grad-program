<a class="toggle-sidebar d-sm-inline d-md-none d-lg-none border-0">
  <i class="material-icons">&#xE5C4;</i>
</a>
<div class="container-fluid text-center my-3">
  <h6><b>AdNU Graduate Program</b></h6>
</div>

<div class="container-fluid pt-2 pb-5 border-bottom">
  <img src="assets\img\logo.png" alt="ADNU logo" height=144px width=144px>
</div>

<div class="nav-wrapper">
  <ul class="nav flex-column">
    <li class="nav-item">
      <a class="nav-link " id="Home" href="Home">
      <i class="material-icons">home</i>
      <span>Home</span>
      </a>
    </li>
    <li class="nav-item">
      <a class="nav-link " id="Admission_and_Enrollment" href="Admission_and_Enrollment">
      <i class="material-icons">vertical_split</i>
      <span>Admission and Enrollment</span>
      </a>
    </li>
    <li class="nav-item">
      <a class="nav-link " id="General_Policy" href="General_Policy">
      <i class="material-icons">content_paste</i>
      <span>General Policy</span>
      </a>
    </li>
    <li class="nav-item">
      <a class="nav-link " id="Programs" href="Programs">
      <i class="material-icons">library_books</i>
      <span>Programs</span>
      </a>
    </li>
    <li class="nav-item">
      <a class="nav-link " id="Plan_of_Study" href="Plan_of_Study">
      <i class="material-icons">school</i>
      <span>Plan of Study</span>
      </a>
    </li>
  </ul>
</div>
