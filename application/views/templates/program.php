<div class="main-content-container container-fluid p-2">
  <!-- Page Header -->
  <!-- <div class="page-header row no-gutters py-4">
      <div class="col-12 col-sm-4 text-center text-sm-left mb-0">
          <span class="text-uppercase page-subtitle">Programs</span>
          <h3 class="page-title">Programs Offered</h3>
      </div>
  </div> -->
  <?php
      foreach ($programs->result() as $prog):
        echo"<div class='row'>";
          echo"<div class='col-lg-12 col-md-12 col-sm-12 mb-4'>";
            echo"<div class='card card-small'>";
              echo"<div class='card-header border-bottom'>";
                // echo"<div class='row justify-content-between'>";
                //   echo"<div class='col-md-12'>";
                  echo"<a href='Program?id=$prog->program_code' class='float-left'>
                  <h5 class='m-0'>$prog->program_name <small>($prog->program_code)</small></h5></a>";
                  if($user != NULL){
                    echo "<a href='form_reader\delete_programs?id=".$prog->program_code."' class='btn btn-danger btn-sm float-right'><i class='material-icons'>delete</i>Delete</a>";
                  }
                //   echo"</div>";
                // echo"</div>";
              echo"</div>";
              echo"<div class='card-body pt-0 '>";
                echo "<br>";
                echo"<p class='text-muted'>".$prog->program_details."</p>";
              echo"</div>";
            echo"</div>";
          echo"</div>";
        echo"</div>";
      endforeach
  ?>
  <div class="row">
    <!-- Student Information -->
    <div class="col-lg-12 col-md-12 col-sm-12 mb-4 text-center">
      <button class="btn btn-outline-primary btn-block" data-toggle="modal" data-target="#myModal">
        <i class="fa fa-plus mr-1"></i>ADD
      </button>
    </div>
    <!-- End Student Information -->
  </div>
</div>

<div class="container">
  <!-- Trigger the modal with a button -->
  <!-- <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">Open Modal</button> -->

  <!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Add Program</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <form action="form_reader/save_programs" method="post">
          <div class="modal-body">
            <div class="form-group">
              <label for="code">Program Code:</label>
              <input type="text" class="form-control" name="Pcode">
            </div>
            <div class="form-group">
              <label for="name">Program Name:</label>
              <input type="text" class="form-control" name="names">
            </div>
            <div class="form-group">
              <label for="desc">Program Description:</label>
              <textarea name="desc" class="form-control"></textarea>
            </div>
          </div>
          <div class="modal-footer">
            <input type="submit" value="Submit" class="btn btn-success">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          </div>
        </form>
      </div>

    </div>
  </div>
</div>
