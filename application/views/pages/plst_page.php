<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <?php include('application\views\templates\head.php') ?>
  <body>
    <div class="wrapper">
      <!-- sidebar -->
      <div class="main-sidebar col-12 col-md-3 col-lg-2 px-0">
        <?php include('application\views\templates\sidebar.php'); ?>
      </div>

      <div class="main-content col-lg-10 col-md-9 col-sm-12 p-0 offset-lg-2 offset-md-3">
        <!-- navbar -->
        <?php include('application\views\templates\navbar.php'); ?>

          <!-- main face -->
          <?php include('application\views\templates\study.php') ?>
      </div>
    </div>


  </body>
</html>
