<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <?php include('application\views\templates\head.php') ?>
  </head>
  <body>
    <div class="wrapper">
      <!-- sidebar -->
      <div class="main-sidebar col-12 col-md-3 col-lg-2 px-0">
        <?php include('application\views\templates\sidebar.php'); ?>
      </div>

      <div class="main-content col-lg-10 col-md-9 col-sm-12 p-0 offset-lg-2 offset-md-3">
        <!-- navbar -->
        <?php include('application\views\templates\navbar.php'); ?>

        <div class="row my-3">
          <!-- main face -->
          <div class="container-fluid col-lg-9 col-md-12 d-inline-block">
            <div class="jumbotron shadow">
              <?php
                if($user != NULL)
                  include('application\views\templates\edit-button.php');
              ?>
              <div class="ql-editor ql-container">
                <?php include('assets\AdmissionProcedure.txt'); ?>
              </div>
            </div>
          </div>
          <!-- redirects -->
          <div class="container-fluid col-lg-3 col-md-12">
            <?php include('application\views\templates\admission-cards.php') ?>
          </div>
        </div>
      </div>
    </div>

    <script type="text/javascript" src="assets\gweg\gweg.js"></script>
  </body>
</html>
