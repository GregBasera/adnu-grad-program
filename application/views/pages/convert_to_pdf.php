<!-- Convert to PDF -->
<div class="container mb-5" style="background: #FFFFFF;">
	<!-- Banner Image -->
	<div class="row">
		<div class="col-sm-12">
			<img src="<?php echo base_url();?>static/img/adnu_banner.jpg" class="img-fluid" alt="">
		</div>
	</div>

	<!-- Header -->
	<div class="row mt-3">
		<div class="col-sm-12 text-center">
			<span><b>PLAN OF STUDY</b></span>
		</div>
		<div class="col-sm-6 mt-3">
			<p>
				<b>Name of Student:</b>
				<?php echo "$lastname, $firstname $mi."; ?>
				<br>
				<b>Degree Sought:</b>
				<?php echo $degrees[$degree]; ?>
			</p>
		</div>
		<div class="col-sm-6 mt-3 text-right">
			<p>
				<b>Track:</b>
				<?php echo $tracks[$track]; ?>
				<br>
			</p>
		</div>
	</div>

	<!-- Subjects -->
	<div class="row">
		<div class="col-sm-12" id="dynamic_content_wrapper">
			<?php for($i = 0; $i < count($school_year_array) - 1; $i++): ?>
				<?php if($subjectCounter[$i] != 0): ?>
				<span><b><?php echo "$semester_array[$i] Semester, SY $school_year_array[$i]"; ?></b></span>
				<table class="mb-4">
					<tr>
						<th>Code</th>
						<th>Title</th>
						<th>Units</th>
						<th>Lab Type</th>
					</tr>

					<?php for($k = 0; $k < $subjectCounter[$i]; $k++): ?>
						<tr>
							<td style="width: 10%;"><?php echo $subjectCode[$k]; ?></td>
							<td><?php echo $subjectTitle[$k]; ?></td>
							<td style="width: 10%;"><?php echo $subjectUnit[$k]; ?></td>
							<td style="width: 10%;"><?php echo $subjects[$subjectCode[$k]]->fee_type; ?></td>
						</tr>
					<?php endfor ?>

					<tr>
						<th colspan='2' class='text-right'>Total</th>
						<th colspan='2'><?php echo $totalUnits[$i]; ?></th>
					</tr>
				</table>

				<?php $sumTotal = 0; ?>

				<div class="container text-center">
					<table class="mb-4 border-0">
						<tr class="border-0">
							<td class="boder-0 text-left">
								<span>Tuition Fee: (<?php echo $totalUnits[$i]; ?> units x Php 1,519.6/unit)</span>
							</td>
							<td class="boder-0 text-right">
								<span><?php $sumTotal += $totalUnits[$i] * 1519.6; echo "Php " . $totalUnits[$i] * 1519.6; ?></span>
							</td>
						</tr>
						<tr class="border-0">
							<td class="boder-0 text-left">
								<span>Other Fees: </span>
							</td>
							<td class="boder-0 text-right">
								<span><?php $sumTotal += 3107.50; echo "Php 3,107.50" ?></span>
							</td>
						</tr>
						<tr class="border-0">
							<td class="boder-0 text-left">
								<span>Other Assessments: </span>
							</td>
							<td class="boder-0 text-right">
								<span><?php $sumTotal += 492.95 ;echo "Php 492.95" ?></span>
							</td>
						</tr>

						<?php if($semester_array[$i] == 'First'): ?>
						<tr class="border-0">
							<td class="boder-0 text-left">
								<span>ID Fee: </span>
							</td>
							<td class="boder-0 text-right">
								<span><?php $sumTotal += 120; echo "Php 120" ?></span>
							</td>
						</tr>
						<?php endif; ?>

						<?php
							$labfee = 0;
							for ($q=0; $q < $subjectCounter[$i]; $q++) {
								if($subjects[$subjectCode[$q]]->fee_type == 'Energy_Fee')
									$labfee += 159.20;
								else if($subjects[$subjectCode[$q]]->fee_type == 'DIA_Laboratory Fee')
									$labfee += 6013.65;
								else if($subjects[$subjectCode[$q]]->fee_type == 'EDP Laboratory Fee')
									$labfee += 4129.35;
							}
						 ?>

						<tr class="border-0">
							<td class="boder-0 text-left">
								<span>Energy Fee: </span>
							</td>
							<td class="boder-0 text-left">
								<span>Php 159.20 / subject</span>
							</td>
						</tr>

						<?php if($degrees[$degree] == 'Master of Information Technology' || $degrees[$degree] == 'Master of Science in Computer Science'): ?>
						<tr class="border-0">
							<td class="boder-0 text-left">
								<span>EDP Laboratory Fee: </span>
							</td>
							<td class="boder-0 text-left">
								<span> Php 4129.35 / subject</span>
							</td>
						</tr>
						<?php endif; ?>

						<?php if($degrees[$degree] == 'Master of Computer Graphics'): ?>
						<tr class="border-0">
							<td class="boder-0 text-left">
								<span>DIA Laboratory Fee: </span>
							</td>
							<td class="boder-0 text-left">
								<span> Php 6013.65 / subject</span>
							</td>
						</tr>
						<?php endif; ?>

						<tr class="border-0">
							<td class="boder-0 text-left">
								<span>Labaratory Fee: </span>
							</td>
							<td class="boder-0 text-right">
								<span><?php $sumTotal += $labfee; echo $labfee; ?></span>
							</td>
						</tr>

						<tr class="border-0">
							<th>Total: </th>
							<td class="boder-0 text-right">
								<span><?php echo "Php. " . $sumTotal; ?></span>
							</td>
						</tr>
					</table>
				</div>

				<?php endif; ?>
			<?php endfor ?>
		</div>

		<!-- Dynamic Contents -->
		<?php // echo $pdf_content; ?>
		<!-- End Dynamic Contents -->

	</div>

	<!-- Footer -->
	<div class="row mt-5">
		<div class="col-sm-4">
			<span class="d-block pb-5">Prepared By:</span>
			<br>
			<span>
				<b>Jelly P. Aureus, MS</b>
			</span>
			<br>
			<span>Program Coordinator</span>
			<br>
		</div>
		<div class="col-sm-6">
			<span class="d-block pb-5">Prepared By:</span>
			<br>
			<span>
				<b>Rebecca C. Torres, Ph.D.</b>
			</span>
			<br>
			<span>Grant Management Administrator</span>
			<br>
			<span>CHED K-12 Transition Scholarship Program</span>
			<br>
		</div>
	</div>
	<div class="row mt-5">
		<div class="col-sm-4">
			<span class="d-block pb-5">Approved By:</span>
			<br>
			<span>
				<b>Marlinda S. Regondola, Ph.D.</b>
			</span>
			<br>
			<span>OIC-Dean, College of Computer Studies</span>
			<br>
		</div>
		<div class="col-sm-6">
			<span class="d-block pb-5">Approved By:</span>
			<br>
			<span>
				<b>Michael A. Cuesta, Ph.D.</b>
			</span>
			<br>
			<span>Dean, Graduate School</span>
			<br>
		</div>
	</div>
	<div class="row mt-5">
		<div class="col-sm-4">
			<span class="d-block pb-5">CONFORME:</span>
			<br>
			<span>
				<b>
					<?php echo "$firstname $mi. $lastname"; ?>
				</b>
			</span>
			<br>
			<span>CHED Scholar</span>
			<br>
			<span>
				<?php echo date("M d, Y"); ?>
			</span>
			<br>
		</div>
	</div>


</div>

<!-- Auto Print the Page -->
<script>
	$(function () {
		window.print();
	});
</script>
