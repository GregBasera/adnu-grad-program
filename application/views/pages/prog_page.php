<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <?php include('application\views\templates\head.php'); ?>
  </head>
  <body>
    <div class="wrapper">
      <!-- sidebar -->
      <div class="main-sidebar col-12 col-md-3 col-lg-2 px-0">
        <?php include('application\views\templates\sidebar.php'); ?>
      </div>

      <div class="main-content col-lg-10 col-md-9 col-sm-12 p-0 offset-lg-2 offset-md-3">
        <!-- navbar -->
        <?php include('application\views\templates\navbar.php'); ?>

        <div class="row my-3">
          <?php include('application\views\templates\Programs_details.php'); ?>
        </div>
      </div>
    </div>

    <script><?php include('assets\gweg\active.js'); ?></script>
  </body>
</html>
