<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <?php include('application\views\templates\head.php') ?>
  </head>
  <body>
    <!-- navbar -->
    <div class="navbar w-100">
      <h4><?php echo $title ?></h4>
    </div>
    <!-- main face -->
    <div class="login-main w-100 p-5">
      <div class="container">
        <img src="<?php echo base_url() ?>assets\img\logo.png" alt="Adnu Logo"  height=144px width=144px>
        <h3 class="text-center p-4 border-bottom">Welcome to ADNU CSS</h3>
        <div class="row">
          <div class="col border-right pt-5">
            <form action="Login/guest" method="post">
              <button class="btn btn-secondary btn-lg m-auto" type="submit" name="guestTunnel">Login as Guest</button>
            </form>
          </div>
          <div class="col border-left">
            <form action="Login/validate" method="post">
              <h4 class="text-center m-3">Login as Admin</h4>
              <input class="form-control form-control-lg" type="text" name="username" placeholder="Username" required>
              <input class="form-control form-control-lg" type="password" name="pass" placeholder="Password" required>
              <div class="my-4">
                <button class="btn btn-primary m-auto" type="submit" name="signin">Sign In</button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
    <!-- footer -->
    <div class="login-footer w-100 bg-primary"></div>
  </body>
</html>
