print assesment
<!-- Convert to PDF -->
<div class="container mb-5" style="background: #FFFFFF;">
	<!-- Banner Image -->
	<div class="row">
		<div class="col-sm-12">
			<img src="<?php echo base_url();?>static/img/adnu_banner.jpg" class="img-fluid" alt="">
		</div>
	</div>

	<!-- Header -->
	<div class="row mt-3">
		<div class="col-sm-12 text-center">
			<span><b>ASSESSMENT</b></span>
		</div>
		<div class="col-sm-6 mt-3">
			<p>
				<b>Name of Student:</b>
				<?php echo "$lastname, $firstname $mi."; ?>
				<br>
				<b>Degree Sought:</b>
				<?php echo $degrees[$degree]; ?>
			</p>
		</div>
		<div class="col-sm-6 mt-3 text-right">
			<p>
				<b>Track:</b>
				<?php echo $tracks[$track]; ?>
				<br>
			</p>
		</div>
	</div>

	<!-- Subjects -->
	<div class="row">
		<div class="col-sm-12" id="dynamic_content_wrapper">
			<?php for($i = 0; $i < count($school_year_array) - 1; $i++): ?>
				<?php if($subjectCounter[$i] != 0): ?>
				<span><b><?php echo "$semester_array[$i] Semester, SY $school_year_array[$i]"; ?></b></span>
				<table class="mb-4">
					<tr>
						<th>Code</th>
						<th>Title</th>
						<th>Units</th>
					</tr>
						<?php for($k = 0; $k < $subjectCounter[$i]; $k++): ?>
						<tr>
							<td style="width: 10%;"><?php echo $subjectCode[$k]; ?></td>
							<td><?php echo $subjectTitle[$k]; ?></td>
							<td style="width: 10%;"><?php echo $subjectUnit[$k]; ?></td>
						</tr>
						<?php endfor ?>
					<tr>
						<th colspan='2' class='text-right'>Total</th>
						<th><?php echo $totalUnits[$i]; ?></th>
					</tr>
				</table>
				<?php endif; ?>
			<?php endfor ?>
		</div>

		<!-- Dynamic Contents -->
		<?php // echo $pdf_content; ?>
		<!-- End Dynamic Contents -->

	</div>

	<!-- Footer -->
	<div class="row mt-5">
		<div class="col-sm-4">
			<span class="d-block pb-5">Prepared By:</span>
			<br>
			<span>
				<b>Jelly P. Aureus, MS</b>
			</span>
			<br>
			<span>Program Coordinator</span>
			<br>
		</div>
		<div class="col-sm-6">
			<span class="d-block pb-5">Prepared By:</span>
			<br>
			<span>
				<b>Rebecca C. Torres, Ph.D.</b>
			</span>
			<br>
			<span>Grant Management Administrator</span>
			<br>
			<span>CHED K-12 Transition Scholarship Program</span>
			<br>
		</div>
	</div>
	<div class="row mt-5">
		<div class="col-sm-4">
			<span class="d-block pb-5">Approved By:</span>
			<br>
			<span>
				<b>Marlinda S. Regondola, Ph.D.</b>
			</span>
			<br>
			<span>OIC-Dean, College of Computer Studies</span>
			<br>
		</div>
		<div class="col-sm-6">
			<span class="d-block pb-5">Approved By:</span>
			<br>
			<span>
				<b>Michael A. Cuesta, Ph.D.</b>
			</span>
			<br>
			<span>Dean, Graduate School</span>
			<br>
		</div>
	</div>
	<div class="row mt-5">
		<div class="col-sm-4">
			<span class="d-block pb-5">CONFORME:</span>
			<br>
			<span>
				<b>
					<?php echo "$firstname $mi. $lastname"; ?>
				</b>
			</span>
			<br>
			<span>CHED Scholar</span>
			<br>
			<span>
				<?php echo date("M d, Y"); ?>
			</span>
			<br>
		</div>
	</div>
</div>

<!-- Auto Print the Page -->
<script>
	$(function () {
		window.print();
	});
</script>
