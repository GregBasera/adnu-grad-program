var currentUrl = window.location.href;
var partsUrl = currentUrl.split("/");
var partsFile = partsUrl[5].split(".");
var targetID = partsFile[0];

if(targetID.substring(0, 8) == 'Program?')
  targetID = 'Programs';

var classList = document.getElementById(targetID).classList;
classList.add("active");
