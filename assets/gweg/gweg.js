var currentUrl = window.location.href;
var partsUrl = currentUrl.split("/");
var partsFile = partsUrl[5].split(".");
var targetID = partsFile[0];

var classList = document.getElementById(targetID).classList;
classList.add("active");
// document.getElementById("navBrand").innerHTML = targetID.replace(/_/g, " ");

$("#spinner1").hide();
$("#spinner2").hide();
$("#spinner3").hide();

var toolbarOptions = [
[{ 'font': [] }],
[{ 'size': [ 'small', false, 'large', 'huge' ]}],
[{ 'align': [] }],
['bold', 'italic', 'underline', 'strike'],
[{ 'color': [] }, { 'background': [] }],
[{ 'header': 1 }, { 'header': 2 }],
[{ 'list': 'ordered'}, { 'list': 'bullet' }],
['link', 'image']
];

var ql = new Quill('#add-ql', {
  modules: {
    syntax: false,
    toolbar: toolbarOptions
  },
  placeholder: "Announcements here",
  theme: 'snow'
});

var quill = new Quill('#editor-container', {
  modules: {
    syntax: false,
    toolbar: toolbarOptions
  },
  placeholder: "Announcements here",
  theme: 'snow'
});

function newtabview(x){
  x = "assets/downloadable/" + x;
  var win = window.open(x, '_blank');
  win.focus();
}

function redirect(x){
  var win = window.open(x, '_blank');
  win.focus();
}

function Ddelete(file){
  var fpath = "../downloadable/" + file;
  $.ajax({url:"assets/php/delete.php", type:"GET", data:{fname: fpath}});
  setTimeout(function(){ window.location.reload(true); }, 3000);
}

var ind = 0;

function editModal(insert, index){
  insert = insert.replace(/~@/g, "\"");
  insert = insert.replace(/@~/g, "'");
  quill.root.innerHTML = insert;
  ind = index;
}

function store(ask){
  var data = quill.root.innerHTML;
  $.ajax({url:"assets/php/post.php", type:"POST", data:{qldata: data, index: ind, location: ask}});
  $("#spinner1").fadeIn(500);
  setTimeout(function(){ window.location.reload(true); }, 3000);
}

function createAnnouncement(){
  var data = ql.root.innerHTML;
  $.ajax({url:"assets/php/insert-top.php", type:"POST", data:{qldata: data}});
  $("#spinner2").fadeIn(500);
  setTimeout(function(){ window.location.reload(true); }, 3000);
}

function delAnnounce(index){
  var insert = "<h6>Delete Announcement No. " + index + "</h6>";
  deleteAnn.innerHTML = insert;
  ind = index;
}

function deleteAnnouncement(){
  $.ajax({url:"assets/php/deleteAnnounce.php", type:"POST", data:{index: ind}});
  $("#spinner3").fadeIn(500);
  setTimeout(function(){ window.location.reload(true); }, 3000);
}
