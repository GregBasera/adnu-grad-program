<?php
  // $logon = false;

  function getUrl(){
    $fullUrl = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";

    return $fullUrl;
  }

  function confirmDelete($file, $q){
    echo "
      <div class='modal fade' id='confirmDelete$q' tabindex='-1' role='dialog' aria-labelledby='confirmDeeletemodal' aria-hidden='true'>
        <div class='modal-dialog' role='document'>
          <div class='modal-content'>
            <div class='modal-header'>
              <h5 class='modal-title' id='confirmDeeletemodal'>Confirm</h5>
              <button type='button' class='close' data-dismiss='modal' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
            </div>
            <div class='modal-body'>
              <font color='red'>$file</font> will be deleted.
            </div>
            <div class='modal-footer'>
              <button type='button' class='btn btn-secondary' data-dismiss='modal'>cancel</button>
              <button type='button' class='btn btn-primary' onclick='Ddelete(\"$file\")'>Confirm</button>
            </div>
          </div>
        </div>
      </div>
    ";
  }

  function kwillmodule(){
    echo "<div id='editor-container'>";

    if(strpos(getUrl(), "Home") == true){
      // echo "<div id='editor-container'></div>";
    }
    if(strpos(getUrl(), "Admission_and_Enrollment") == true){
      include('assets\AdmissionProcedure.txt');
    }
    if(strpos(getUrl(), "General_Policy") == true){
      include('assets\GeneralPolicy.txt');
    }

    echo "</div>";
  }

  // if(strpos(getUrl(), "log=1") == true)
  //   $logon = true;
?>
